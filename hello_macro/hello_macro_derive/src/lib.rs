// At this point procedural macros need to be in their own crate.
// (Mentioned in src/macros/macros.rs)
// Hopefully this restriction will be lifted in the future.
use proc_macro::TokenStream;
use quote::quote;
use syn;

// The proc_macro crate is the compiler’s API that allows us to read and manipulate Rust code from our code.
//
// The syn crate parses Rust code from a string into a data structure that we can perform operations on.
// The quote crate turns syn data structures back into Rust code.

#[proc_macro_derive(HelloMacro)]
pub fn hello_macro_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_hello_macro(&ast)
}

fn impl_hello_macro(ast: &syn::DeriveInput) -> TokenStream {
    // DeriveInput {
    //     // --snip--
    //
    //     ident: Ident {
    //         ident: "Pancakes",
    //         span: #0 bytes(95..103)
    //     },
    //     data: Struct(
    //         DataStruct {
    //             struct_token: Struct,
    //             fields: Unit,
    //             semi_token: Some(
    //                 Semi
    //             )
    //         }
    //     )
    // }

    let name = &ast.ident;
    let gen = quote! {
        impl HelloMacro for #name {
            fn hello_macro() {
                println!("Hello, Macro! My name is {}!", stringify!(#name));
            }
        }
    };

    gen.into()
}
