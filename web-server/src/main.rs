use first_utils::ThreadPool;
use std::fs;
use std::io::{BufRead, BufReader, Write};
use std::net::{TcpListener, TcpStream};
use std::time::Duration;

const PORT: u16 = 7878;

fn main() {
    println!("Toy WebServer is starting...");
    let pool = ThreadPool::new(4);

    let listener = TcpListener::bind(format!("127.0.0.1:{}", PORT)).unwrap();
    println!("Server is listening on port {}", PORT);

    // for stream in listener.incoming().take(2) {
    for stream in listener.incoming() {
        let stream = stream.unwrap();

        pool.execute(|| {
            handle_connection(stream);
        });
    }

    println!("Logic ended.");
}

fn handle_connection(mut stream: TcpStream) {
    let buf_reader = BufReader::new(&mut stream);
    let request_line = buf_reader.lines().next().unwrap();

    if request_line.is_err() {
        println!("Whoa! Someone closed the browser?!");
        return;
    }

    let request_line = request_line.unwrap();

    // Ok let's make it a bit simpler
    let (status_line, filename) = match &request_line[..] {
        "GET / HTTP/1.1" => ("HTTP/1.1 200 OK", "web-server/default_response.html"),
        "GET /sleep HTTP/1.1" => {
            println!("Sleep start");
            std::thread::sleep(Duration::from_secs(5));
            println!("Sleep done");
            ("HTTP/1.1 200 OK", "web-server/default_response.html")
        }
        _ => ("HTTP/1.1 404 NOT FOUND", "web-server/404.html"),
    };

    // :#? <- pretty debug formatting
    // println!("Request: {:#?}", http_request);

    let contents = fs::read_to_string(filename).unwrap();
    let length = contents.len();

    let response = format!("{status_line}\r\nContent-Length: {length}\r\n\r\n{contents}");

    stream.write_all(response.as_bytes()).unwrap();
}
