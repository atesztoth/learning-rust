mod common;

// https://doc.rust-lang.org/book/ch11-03-test-organization.html#integration-tests-for-binary-crates

#[test]
fn test_me() {
    common::setup();
    assert_eq!(rust_learn::adder(2), 3)
}
