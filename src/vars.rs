static LANGUAGE: &str = "Rust";

fn main() -> () {
    // constants cannot be shadowed
    const FRIENDLIEST_ANIMAL_NAME: &str = "Dog";

    // Constants can be assigned compile time evaluatable expressions. Obviously it does not include variables.
    const HOURS_IN_DAY: u32 = 24;
    const HOUR_IN_SECONDS: u32 = 60 * 60;
    const DAY_IN_SECONDS: u32 = HOUR_IN_SECONDS * HOURS_IN_DAY;

    // shadowing is enabled.
    let x = 1;
    let x = x + 5;

    // Oh yeah and a very interesting concept:
    let change_my_type = 5 * 5;
    let change_my_type: String = change_my_type.to_string();

    println!("x is {}. changed type {}", x, change_my_type);
}
