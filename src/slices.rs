// section 4

pub fn main() {
    // Slices lets you get a portion of an iterable that has an immutable ref to it.
    // This way bugs, like storing a position in a variable related to a string then the string changes
    // can be avoided.

    // Slices have a generic implementation:
    let a = [1, 2, 3, 4, 5];

    let slice = &a[1..3];

    assert_eq!(slice, &[2, 3]);

    let example_string = String::from("sample string");

    let slice_till_space = get_first_string_chunk(&example_string);

    println!("Hey, look at this! The first slice!");
    println!("{}", slice_till_space);
}

fn get_first_string_chunk(str: &str) -> &str {
    let s = str.as_bytes();

    // Destructuring the enumerate result (index, immutable reference to a byte)
    for (i, &elem) in s.iter().enumerate() {
        // Byte literal syntax
        if elem != b' ' {
            continue;
        }

        // Got a space, let's return a slice till that
        return &str[..i];
    }

    // No space, returning the whole str
    return &str[..];
}
