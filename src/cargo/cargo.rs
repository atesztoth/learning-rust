pub fn main() {
    // There are two profiles added by default for building code.
    // The first is the development profile (dev) which is used
    // when cargo build is ran, and then there is the release profile
    // that is used when cargo build --release is used.

    // New profiles or profile customization can be done in cargo.toml.
    // Use [profile.*] sections.
    // E.g.:
    // [profile.dev]
    // opt-level = 0
    //
    // [profile.release]
    // opt-level = 3

    println!("add_one {}", add_one(1));
}

// This 3 / comments are called documentation comments and the support
// markdown notation. Rust can generate HTML documentation using these using "cargo doc".
// try: cargo doc --open . This will open the html containing the docs for current code.

// cargo test will run the tests in Examples block.

/// Adds one to the number given.
///
/// # Examples
///
/// ```
/// let arg = 5;
/// let answer = my_crate::add_one(arg);
///
/// assert_eq!(6, answer);
/// ```
pub fn add_one(x: i32) -> i32 {
    x + 1
}

// Oh, and don't forget, with pub use <something> one can re-export types stuff etc.
// Other stuff in the book I won't take note of here.
// The examples are just perfect in the book, no need to summarize the content etc.
// Read: https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html
