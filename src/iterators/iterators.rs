// Iterators are lazy in rust. This means until they are not used they are just chilling.

pub fn main() {
    let v1 = vec![1, 2, 3];
    let v1_iter = v1.iter();

    // use the iterator with a for loop:
    for val in v1_iter {
        println!("Val: {val}")
    }
    // https://doc.rust-lang.org/book/ch13-02-iterators.html#the-iterator-trait-and-the-next-method

    // Notes:
    // When calling "next()" of an iterator we get back immutable references to values.
    // If we want to create iterators that take ownership of the iterable and returns owned values:
    let v2 = vec![1, 2];
    let _v2_owned_iter = v2.into_iter(); // took ownership of v2, returns owned values.

    // To iterate over mutable references use:
    let mut v3 = vec![3, 4];
    let mut mutable_ref_iterator = v3.iter_mut();
    let v3_elem = mutable_ref_iterator.next();
    // v3_elem is an Option<&mut i32>, we need the mutable reference from it, so let's get it in
    // some ways:
    // First, by pattern matching:
    // match v3_elem {
    //     None => {}
    //     Some(num) => {
    //         // Note the dereference operator!
    //         *num = 100
    //     }
    // }

    // other way:
    *v3_elem.unwrap() = 400;
    // What happened here? Since I am sure the Option type of v3_elem HAS a value in it, just unwrapped
    // the value, then referenced it and assigned a new value to it. That's it.
    dbg!(v3);

    // Methods that call "next" are called consuming adaptors, because calling them consumes the iterator.
    // One example is the sum method, which takes ownership of the iterator
    // and iterates through the items by repeatedly calling next, thus consuming the iterator.
    let v4 = vec![1, 2, 3, 4, 5];
    let sum: i32 = v4.iter().sum();
    dbg!(sum);

    // Iterator adaptors are methods defined on the Iterator trait that don’t consume the iterator.
    // Instead, they produce different iterators by changing some aspect of the original iterator.

    // "map" is an iterator adaptor that returns a new iterator that produces the modified items.
    // (Map applies the supplied closure on items as the collection is being iterated through.
    let v5 = vec![1, 2, 3, 4, 5];
    // Calling "collect" will consume the iterator!
    let v5_mod: Vec<i32> = v5.iter().map(|v| v + 1).map(|x| x + 1).collect();
    dbg!(v5_mod);
    // Notice above the maps has been changed together, the point is .collect needed to be called
    // in order to have the iterators consumed!

    // let nums = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];
    // let res = filter_odds(nums);
    // dbg!(res);
    // dbg!(nums); // nums moved into filter_odds, cannot be used again.

    let res = filter_odds(vec![1, 2, 3, 4, 5, 6, 7, 8, 9]);
    dbg!(res);

    // Iterators follow zero-overhead principle regarding implementation,
    // so is equally performant as a for loop, sometimes even more.
}

fn filter_odds(nums: Vec<i32>) -> Vec<i32> {
    nums.into_iter().filter(|x| x % 2 == 0).collect()
}

#[test]
fn iterator_demonstration() {
    let v1 = vec![1, 2, 3];

    let mut v1_iter = v1.iter();

    assert_eq!(v1_iter.next(), Some(&1));
    assert_eq!(v1_iter.next(), Some(&2));
    assert_eq!(v1_iter.next(), Some(&3));
    assert_eq!(v1_iter.next(), None);
}
