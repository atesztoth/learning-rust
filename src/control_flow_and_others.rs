fn main() -> () {
    // in if's argument list the parenthesis can be omitted
    // also, since if is an expression, it can be assigned to variables. It is an rvalue.
    // The code blocks to be executed after evaluating the predicate called arms.

    // Oh, and an extra, number literals are expressions by themselves, that is why they can be
    // returned from functions. It was just a note.
    let value = if 4 < 5 { 4 } else { 5 };

    println!("{}", value);
}
