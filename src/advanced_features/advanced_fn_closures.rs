use std::fmt;
use std::fmt::{Debug, Display, Formatter};

pub fn main() {
    example_fn_pointers();
    example_returning_closures();
}

fn example_fn_pointers() {
    // Function pointers have the type of fn, not Fn, which is the closure trait.

    fn add_one(x: i32) -> i32 {
        x + 1
    }

    fn do_twice(f: fn(i32) -> i32, arg: i32) -> i32 {
        f(arg) + f(arg)
    }

    let result = do_twice(add_one, 5);
    println!("do_twice result: {}", result);

    // Function pointers implement all three of the closure traits (Fn, FnMut, and FnOnce),
    // meaning you can always pass a function pointer as an argument for a function that expects a closure.
    // It’s best to write functions using a generic type
    // and one of the closure traits so your functions can accept either functions or closures.

    // !!!
    // A use-case when only accepting fns but no closures is mandatory when interfacing with
    // some external code that does not have closures, e.g. C.

    // Example of function that accept both closures and functions:
    let list_of_numbers = vec![1, 2, 3];

    let list_of_strings: Vec<String> = list_of_numbers.iter().map(|i| i.to_string()).collect();
    let list_of_strings2: Vec<String> = list_of_numbers.iter().map(ToString::to_string).collect();

    println!("los: {:?}", list_of_strings);
    println!("los2: {:?}", list_of_strings2);

    // Enum values also become initializer functions. (So calling it as the ctor of the type is actually
    // extra valid :D ).

    enum Status {
        Value(u32),
        Stop,
    }

    impl Display for Status {
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            match self {
                Status::Value(v) => {
                    write!(f, "Value({})", &v)
                }
                Status::Stop => {
                    write!(f, "Stop")
                }
            }
        }
    }

    impl Debug for Status {
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            <Self as Display>::fmt(&self, f)
        }
    }

    let list_of_statuses: Vec<Status> = (0u32..20).map(Status::Value).collect();

    // Here we create Status::Value instances using each u32 value in the range that map is called
    // on by using the initializer function of Status::Value.
    // Some people prefer this style, and some people prefer to use closures.
    // --> They compile to the same code, so use whichever style is clearer to you. <--

    println!("List of statuses {:?}", list_of_statuses);
}

fn example_returning_closures() {
    // Closures cannot be returned directly.
    // This is because they are represented by traits.
    // When possible it's better to return the concrete type that implements the trait.

    // However, using dynamic dispatch and some heap allocation through the Box trait,
    // it's doable. So returning it as a trait object.

    // Will not compile, size is not known at compile time. It's not "Sized".
    // fn returns_closure() -> dyn Fn(i32) -> i32 {
    //     |x| x + 1
    // }

    fn create_incrementer() -> Box<dyn Fn(i32) -> i32> {
        Box::new(|x| x + 1)
    }

    let c = create_incrementer();
    println!("Using returned closure {}", (*c)(1));

    // let incrementer  = *(create_incrementer());
    // println!("Also with a syntactically better form {}", incrementer(1));

    // ajajajajjj this caused a lotta problems, it's not sized. U don't say Sherlock?
    // let incrementer = *(create_incrementer());
    // println!("Also with a syntactically better form {}", incrementer(1));

    // Will play with this maybe later, no time now.
    // The problem is the size cannot be known at compile time for the variable.
    // Okay I'll let it go for now, it is way more important to finish the book.
}
