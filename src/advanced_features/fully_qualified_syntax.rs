use std::fmt;
use std::fmt::{write, Formatter};

pub fn main() {
    example_conflicting_method_names();
    example_supertrait();
    newtype_pattern();
    example_trait_ambiguity();
}

fn example_conflicting_method_names() {
    // In case there is some ambiguity in which method to use,
    // for example implementing two traits with the same method for the same struct,
    // or even the Struct could have a method with that name, we need to be specific
    // what method to use.

    trait Pilot {
        fn fly(&self);
    }

    trait Wizard {
        fn fly(&self);
    }

    struct Human;

    impl Pilot for Human {
        fn fly(&self) {
            println!("This is your captain speaking.");
        }
    }

    impl Wizard for Human {
        fn fly(&self) {
            println!("Up!");
        }
    }

    impl Human {
        fn fly(&self) {
            println!("*waving arms furiously*");
        }
    }

    let person = Human;
    person.fly(); // calls fly method of Human (seems like a logical default to me)

    println!("--------------");

    // To call the fly methods from either the Pilot trait or the Wizard trait,
    // we need to use more explicit syntax to specify which fly method we mean.

    // Now this syntax tho, bruh... Nyeh...
    Pilot::fly(&person);
    Wizard::fly(&person);
    person.fly();

    // This is still not enough, as this just solves the problem of methods, which takes in
    // &self as the first param.
    // Need a solution for associated methods.
    // This will be the Fully Qualified Syntax.

    trait Animal {
        fn baby_name() -> String;
    }

    struct Dog;

    impl Dog {
        fn baby_name() -> String {
            String::from("Spot")
        }
    }

    impl Animal for Dog {
        fn baby_name() -> String {
            String::from("puppy")
        }
    }

    println!("A baby dog is called a {}", Dog::baby_name());
    // println!("A baby dog is called a {}", Animal::baby_name()); // No good, there would be ambiguity if multiple types implemented Animal.
    // So the syntax is not strict enough.

    // To disambiguate and tell Rust that we want to use the implementation of Animal for Dog
    // as opposed to the implementation of Animal for some other type,
    // we need to use fully qualified syntax.
    println!("A baby dog is called a {} ✅", <Dog as Animal>::baby_name());

    // <Type as Trait>::function(receiver_if_method, next_arg, ...);
    //
    // For associated functions that aren’t methods, there would not be a receiver:
    // there would only be the list of other arguments.
    // You could use fully qualified syntax everywhere that you call functions or methods.
    // However, you’re allowed to omit any part of this syntax that
    // Rust can figure out from other information in the program.
    // You only need to use this more verbose syntax in cases where there are
    // multiple implementations that use the same name and
    // Rust needs help to identify which implementation you want to call.
}

// *** Supertraits
struct Point {
    x: i32,
    y: i32,
}

fn example_supertrait() {
    // Supertraits comes into picture when we have a trait, that depends on another trait.
    // So in cases, when a type implements a trait, it is required to also implement another trait.
    // The "another trait" is called the supertrait of the current trait as it is a requirement
    // for the current trait that the type also has the "another trait" implemented.

    // Following the example of the book:

    trait HelloSayer {
        fn say_hello() {
            println!("Hello!");
        }
    }

    impl HelloSayer for Point {}

    // Let's make Point implement fmt::Display
    #[allow(non_local_definitions)]
    impl fmt::Display for Point {
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            write!(f, "({}, {})", self.x, self.y)
        }
    }

    // Multiple trait requirements can be written using the "+" operator.
    trait OutlinePrint: fmt::Display + HelloSayer {
        fn outline_print(&self) {
            let output = self.to_string();
            let len = output.len();

            println!("{}", "*".repeat(len + 4));
            println!("*{}*", " ".repeat(len + 2));
            println!("* {} *", output);
            println!("*{}*", " ".repeat(len + 2));
            println!("{}", "*".repeat(len + 4));
        }
    }

    impl OutlinePrint for Point {}

    let p = Point { x: 20, y: 15 };
    p.outline_print();
    Point::say_hello();
}

// *** Using the Newtype Pattern to Implement External Traits on External Types
// "Newtype is a term that originates from the Haskell programming language." I knew that!! I knew that right away! :D
fn newtype_pattern() {
    // I forgot this lol. So there is a rule, the orphan rule that states that one is only allowed to
    // implement a trait on a type if either the trait or the type are local to the current crate.

    // SO to get around it, the `newtype` pattern comes in.
    // This involves creating a new type in a tuple struct. (Where is this going? :thinking:)
    // "The tuple struct will have one field and be a thin wrapper around the type
    // we want to implement a trait for." -- So in this case the type is getting "brought into" the crate.
    //
    // And yeah, then the trait will be implemented for this wrapper type.
    // Important: it's a zero cost thing, no runtime penalty for it. The wrapper type is elided
    // at compile time.

    // Example: implementing Display on Vec<T>.

    // Let's see the problem:
    // impl<T> fmt::Display for Vec<T> {}; // Only traits defined in the current crate can be implemented for arbitrary types [E0117]

    // LEMME HACK
    struct StringVecWrapper(Vec<String>);

    impl fmt::Display for StringVecWrapper {
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            write!(f, "[{}]", self.0.join(", "))
        }
    }

    let w = StringVecWrapper(vec![String::from("hello"), "second string".to_owned()]);
    println!("w = {}", w);

    // Since this new type is a wrapper around the Vec<String> it does not have the methods of
    // Vec. In order to "get" these methods, one would need to implement them all and
    // delegate the calls to the wrapped type. So actually one cannot "get" these methods like ...
    // in the sense of "inheriting", but to like... "conform" to the original type by delegation.

    // One interesting thing: by implementing Deref on the wrapper type in the way that it returns
    // the wrapped type, this would do the trick to have every method. (Because it actually returns
    // the wrapped object.....)
}

// There were one very interesting example in the book.
//
// mod inner {
//     pub trait A {
//         fn f(&self) -> usize { 0 }
//     }
//     pub trait B {
//         fn f(&self) -> usize { 1 }
//     }
//     pub struct P;
//     impl A for P {}
//     impl B for P {}
// }
// fn main() {
//     use inner::{P, B};
//     println!("{}", P.f());
// }

// So in this case clearly A and B traits create ambiguity.
// BUT when the type, and only one trait is imported: use inner::{P, B}
// Then the imported trait counts only, so this program would output 1.
// Let's try it:

mod inner {
    pub trait A {
        fn f(&self) -> usize {
            0
        }
    }
    pub trait B {
        fn f(&self) -> usize {
            1
        }
    }
    pub struct P;
    impl A for P {}
    impl B for P {}
}

fn example_trait_ambiguity() {
    use inner::{B, P};
    println!("{}", P.f()); // 1 , wow.
}
