use std::{mem, slice};

/**
*  To switch to unsafe Rust, use the unsafe keyword and then start a new block that holds the unsafe code.
*  You can take five actions in unsafe Rust that you can’t in safe Rust,
*  which we call unsafe superpowers. Those superpowers include the ability to:

*  Dereference a raw pointer
*  Call an unsafe function or method
*  Access or modify a mutable static variable
*  Implement an unsafe trait
*  Access fields of unions
 */

pub fn main() {
    println!("19/1: Unsafe Rust.");

    dereference_raw_pointer();
    unsafe_fn_example();
    example_extern();
    example_global_mut_change();
    unsafe_casts();
}

fn dereference_raw_pointer() {
    // Dereferencing a Raw Pointer
    //
    // Unsafe Rust has two new types called raw pointers that are similar to references.
    // As with references, raw pointers can be immutable or mutable and are written as *const T and *mut T, respectively.
    // The asterisk isn’t the dereference operator; it’s part of the type name. In the context of raw pointers,
    // immutable means that the pointer can’t be directly assigned to after being dereferenced.

    // Different from references and smart pointers, raw pointers:
    //
    //     Are allowed to ignore the borrowing rules by having both immutable and mutable pointers or multiple mutable pointers to the same location
    //     Aren’t guaranteed to point to valid memory
    //     Are allowed to be null
    //     Don’t implement any automatic cleanup

    let mut n = 5;

    let r1 = &n as *const i32; // a const int32 raw pointer
    let r2 = &mut n as *mut i32; // a mutable int32 raw pointer

    // To demonstrate this, next we’ll create a raw pointer whose validity we can’t be so certain of.
    // Listing 19-2 shows how to create a raw pointer to an arbitrary location in memory.
    // Trying to use arbitrary memory is undefined: there might be data at that address or there might not,
    // the compiler might optimize the code so there is no memory access, or the program might error with a segmentation fault.
    // Usually, there is no good reason to write code like this, but it is possible.

    // This is how we can create a raw pointer to any location:
    let address = 0x012345usize;
    let _r = address as *const i32;

    // Dereferencing raw pointers require unsafe block!
    // println!("{}", *r1);

    unsafe {
        println!("r1: {}", *r1);
        println!("r2: {}", *r2);
        // *r1 = 5; // Cannot assign a new value to `immutable dereference of raw pointer`
        *r2 = 5; // 😈
                 // println!("r3: {}", *_r); // Why does it feel so good to cause a segfault? (sigsegv).
    }
    println!("😈 {}", n);

    // Unsafe fns are functions that have some requirements that needs to be upheld when calling them.
    unsafe fn unsafe_example() -> u8 {
        42
    }

    // unsafe_example(); // these cannot be called without an unsafe block

    unsafe {
        let res = unsafe_example();
        println!("{}", res);
    }
}

fn unsafe_fn_example() {
    // There is a very nice example of a function that needs to use unsafe code to do what it wants.
    // This function is actually in the standard lib!

    // [This code does not compile!]
    // Double borrow happens! 2 mutable borrows. In this case this is actually safe.
    // Why? Because the two slices do not overlap.
    // fn split_at_mut(values: &mut [i32], mid: usize) -> (&mut [i32], &mut [i32]) {
    //     let len = values.len();
    //
    //     assert!(mid <= len);
    //
    //     (&mut values[..mid], &mut values[mid..])
    // }

    fn split_at_mut(values: &mut [i32], mid: usize) -> (&mut [i32], &mut [i32]) {
        let len = values.len();
        let pointer = values.as_mut_ptr();

        assert!(mid <= len);

        // This unsafe closure will be wrapped in a safe code.
        unsafe {
            (
                // expects: pointer to the start, and then a length
                slice::from_raw_parts_mut(pointer, mid),
                slice::from_raw_parts_mut(pointer.add(mid), len - mid),
            )
        }

        // The function slice::from_raw_parts_mut is unsafe because it takes a raw pointer
        // and must trust that this pointer is valid. The add method on raw pointers is also unsafe,
        // because it must trust that the offset location is also a valid pointer.
        // Therefore, we had to put an unsafe block around our calls to slice::from_raw_parts_mut
        // and add, so we could call them.
    }
}

extern "C" {
    fn abs(input: i32) -> i32;
}

fn example_extern() {
    // Sometimes, your Rust code might need to interact with code written in another language.
    // For this, Rust has the keyword extern that facilitates the creation and use of a Foreign Function Interface (FFI).
    // An FFI is a way for a programming language to define functions and enable a different
    // (foreign) programming language to call those functions.

    // Within the extern "C" block, we list the names and signatures of
    // external functions from another language we want to call.
    // The "C" part defines which application binary interface (ABI) the external function uses:
    // the ABI defines how to call the function at the assembly level.
    // The "C" ABI is the most common and follows the C programming language’s ABI.

    unsafe { println!("C abs: {}", abs(-3)) }
}

// Making a function callable from another language:
// Adding #[no_mange] is important. Manging means modifying the name of the function while compiling,
// so it has a bit more info for the compiler, but manging is done differently by compilers, so it must be disabled.
#[no_mangle]
pub extern "C" fn special_abs_c(num: i32) -> i32 {
    return num.abs();
}

// In this book, we’ve not yet talked about global variables,
// which Rust does support but can be problematic with Rust’s ownership rules.
// If two threads are accessing the same mutable global variable, it can cause a data race.
static GLOBAL_GREETINGS: &str = "Hello";
// Static variables can only store references with the 'static lifetime,
// which means the Rust compiler can figure out the lifetime, and we aren’t required to annotate it explicitly.
// Accessing an immutable static variable is safe.

// A subtle difference between constants and immutable static variables is that
// values in a static variable have a fixed address in memory.
// Using the value will always access the same data.
// Constants, on the other hand, are allowed to duplicate their data (??) whenever they’re used.
// (What is this talking about?)
//
// Another difference is that static variables can be mutable.
// Accessing and modifying mutable static variables is unsafe.
static mut COUNTER: u32 = 0;

fn add_to_count(inc: u32) {
    unsafe {
        COUNTER += inc;
    }
}

fn example_global_mut_change() {
    add_to_count(10);

    unsafe {
        #![allow(static_mut_refs)]
        println!("COUNTER: {}", COUNTER);
    }
}

fn unsafe_casts() {
    // The transmute function is very simple, but very scary.
    // It tells Rust to treat a value of one type as though it were another type.
    // It does this regardless of the typechecking system, and completely trusts you.

    unsafe {
        // An array of 4bytes treated as an unsigned 32bit int.
        let a = [0u8, 1u8, 0u8, 0u8];
        let b = mem::transmute::<[u8; 4], u32>(a);
        println!("{}", b); // 256
                           // Or, more concisely:
        let c: u32 = mem::transmute(a);
        println!("{}", c); // 256
    }
}
