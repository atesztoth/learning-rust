pub fn main() {
    example_dynamically_sized_types();
}

// *** Creating Type Synonyms with Type Aliases
fn example_type_aliases() {
    // Can declare type aliases like this:
    type Kilometers = i32;

    // But don't forget using a one element struct is better because it nominates the value. Anyway, that should be known.
    let x: i32 = 5;
    let y: Kilometers = 10;

    println!("{}", x + y); // as expected just type aliases so i32 -> i32 -> i32

    type Thunk = Box<dyn Fn() + Send + 'static>;

    let _f: Thunk = Box::new(|| println!("hi"));

    fn takes_long_type(_f: Thunk) {
        todo!()
    }

    fn returns_long_type() -> Thunk {
        todo!()
    }
}

// *** The Never Type that Never Returns

// THE EMPTY TYPE that can be thought about as an empty set.
// How do you get a value of an empty set? THERE IS NO WAY ... and that is the point!

// These are called _diverging functions_ .
// fn bar() -> ! {
//     println!("I'm a diverging function!") // Error, expected '!', found () .
// // Since there is no element of the empty set, we also cannot construct an element of that, hence cannot return.
// }
// When Rust uses "!" is very well explained here: https://rust-book.cs.brown.edu/ch19-04-advanced-types.html#the-never-type-that-never-returns.
// "continue" has "!" type tho.
// type "!" can be coerced to any other type

//    print!("forever ");
//
//     loop {
//         print!("and ever ");
//     }
// A forever looping loop has the return type of "!". If it had a break, the return type would be nothing, I mean
// nothing nothing so "()" type.

// *** Dynamically Sized Types and the Sized Trait
fn example_dynamically_sized_types() {
    // Sometimes it is not possible to know at static time how big a variable needs to be.
    // E.g.: A program may get user input and the user could write a whole novel, why not?

    // So, the size cannot be known at compile time, so how it is done is the memory will be allocated
    // on the heap, and mostly we store some metadata. E.g.: &str, the string slice.
    // It's a combination of the location of the contents in memory (usize) and the length of bytes
    // it takes up (usize again). This should be enough. Very much it should be enough.

    // So this means DSTs are always referred to using some pointer kinda stuff.
    // Pointers are just memory addresses, so in itself that's not enough because we need the size metadata.

    // Every trait is a dynamically sized type.

    // "To work with DSTs, Rust provides the Sized trait to determine whether or not a type’s
    // size is known at compile time.
    // This trait is automatically implemented for everything whose size is known at compile time.
    // In addition, Rust implicitly adds a bound on Sized to every generic function."

    fn tt<T>(_t: T) {}

    // is the same as
    fn ttt<T: Sized>(_t: T) {}

    // !!!!!!!
    // By default, generic functions will work only on types that have a known size at compile time.
    // However, you can use the following special syntax to relax this restriction:
    fn tttt<T: ?Sized>(_t: &T) {
        // work with t
    }

    // A trait bound on ?Sized means “T may or may not be Sized” and this notation overrides
    // the default that generic types must have a known size at compile time.
    // The ?Trait syntax with this meaning is only available for Sized, not any other traits.
    //
    // Also note that we switched the type of the t parameter from T to &T.
    // Because the type might not be Sized, we need to use it behind some kind of pointer.
    // In this case, we’ve chosen a reference.

    fn x<T: Eq + ?Sized>(t: &T, t2: &T) -> bool {
        t == t2
    }

    fn xx(t: &i32, t2: &i32) -> bool {
        println!("{:?}", t as *const _); // getting the mem address
        t == t2
    }

    println!("{}", x(&2, &2));
    println!("{}", x("a", "b"));
    println!("{}", xx(&2, &2));
}
