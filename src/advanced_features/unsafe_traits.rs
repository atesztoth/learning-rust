// Had to start a new file, the previous one was growing too big with too many things.
pub fn main() {
    example_unsafe_trait();
    example_unions();

    let mut v = Vec::with_capacity(3);

    for i in 0..3 {
        v.push(i);
    }

    let n = &v[0] as *const i32;

    // v.push(4); // At this point vec would be reallocated and n would point to deallocated  memory.

    println!("{}", unsafe { *n });
}

fn example_unsafe_trait() {
    // Implementing an Unsafe Trait

    // We can use unsafe to implement an unsafe trait.
    // A trait is unsafe when at least one of its methods has some
    // invariant that the compiler can’t verify.
    // We declare that a trait is unsafe by adding the unsafe keyword before trait
    // and marking the implementation of the trait as unsafe too.

    unsafe trait Foo {
        // methods go here
    }

    unsafe impl Foo for i32 {
        // method implementations go here
    }
}

fn example_unions() {
    // The final action that works only with unsafe is accessing fields of a union.
    // A union is similar to a struct, but only one declared field is used in a particular instance at one time.
    // Unions are primarily used to interface with unions in C code.
    // Accessing union fields is unsafe because
    // Rust can’t guarantee the type of the data currently being stored in the union instance.

    // This is because we can have many members in a union. The size will be equivalent to the size
    // of the largest member, and only 1 field can be filled a time which means only 1 value can be stored there.
    // This is what the last sentence of the previous paragraph refers to.
    #[repr(C)]
    union MyUnion {
        f1: u32,
        f2: f32,
    }

    let u = MyUnion { f1: 2 };

    unsafe {
        println!("{}", u.f1);
    }
}
