use std::fmt::{Display, Formatter};
use std::ops::Add;

pub fn main() {
    example_associated_type();
    example_op_overloading();
    overload_example_2();
}

fn example_associated_type() {
    trait Producer {
        type ProducedType; // example for associated type

        fn produce(&self) -> Self::ProducedType;
    }

    // showing the default generics syntax:
    trait Test<T = String> {
        fn test_produce(&self) -> T;
    }

    // Associated type will be filled for the current implementation.
    struct NumberCreator {}

    impl Producer for NumberCreator {
        type ProducedType = u8;

        fn produce(&self) -> Self::ProducedType {
            1
        }
    }

    // Associated types might seem like a similar concept to generics,
    // in that the latter allow us to define a function without specifying what types it can handle.

    // This was NumberCreator could produce multiple types
    // but this is not what the goal was. Associated type is unique to an implementation.
    impl Test for NumberCreator {
        fn test_produce(&self) -> String {
            String::from("asdf")
        }
    }

    impl Test<i32> for NumberCreator {
        fn test_produce(&self) -> i32 {
            5
        }
    }

    // Not possible. Already implemented once.
    // impl Producer for NumberCreator {
    //     type ProducedType = i32;
    //
    //     fn produce(&self) -> Self::ProducedType {
    //         todo!()
    //     }
    // }

    let nc = NumberCreator {};

    println!("Number creator: {}", nc.produce());

    // Associated types also become part of the trait’s contract:
    // implementors of the trait must provide a type to stand in for the associated type placeholder.
    // Associated types often have a name that describes how the type will be used,
    // and documenting the associated type in the API documentation is good practice.
}

fn example_op_overloading() {
    // Default Generic Type Parameters and Operator Overloading

    // This ia bit of a let-down, but probably these will be enough.
    // Rust `doesn’t allow you to create your own operators or overload arbitrary operators.
    // But you can overload the operations and corresponding traits listed in std::ops.

    #[derive(Debug, Copy, Clone, PartialEq)]
    struct Point {
        x: i32,
        y: i32,
    }

    impl Add for Point {
        type Output = Point;

        fn add(self, other: Point) -> Point {
            Point {
                x: self.x + other.x,
                y: self.y + other.y,
            }
        }
    }

    let x = Point { x: 0, y: 0 };
    let y = Point { x: 10, y: 10 };

    let Point { x, y } = x + y;

    println!("x {}, y {}", x, y);
}

struct Millimeters(u32);

struct Meters(u32);

impl Display for Millimeters {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Milimeters: {}", &self.0)
    }
}

impl Display for Meters {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "Meters: {}", &self.0)
    }
}

impl Add<Meters> for Millimeters {
    type Output = Millimeters;

    fn add(self, other: Meters) -> Millimeters {
        Millimeters(self.0 + (other.0 * 1000))
    }
}

// Previously there were some words about how associated types differ from using generics.
// Now this Add is a great example of the case when we want to use generics and do not want to
// associate a particular type with a particular implementation. We want to be able to provide
// multiple Add implementations with multiple types.

fn overload_example_2() {
    let mm = Millimeters(1000);

    let m = Meters(1);

    println!("Result: {}", mm + m);
}
