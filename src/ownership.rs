/**
 * The tutorial explains everything well, here I'll only mention some things.
 */

pub fn main() {
    // I'll not dive much into the value and reference schematics that Rust uses.
    // But basically anything that implements the Copy trait will be copied, others will be moved.
    // Primitives behaves like values, so will be copied by default:
    let z = 1;
    let _y = z; // copy

    let x1 = String::from("asdf");
    let _x2 = x1;

    // println!("Will not work! x1 is no longer the owner of the String! {}", x1); // borrow of moved value: `x1`
    // Also, x1 is a String, a reference type allocated on the heap. That assignment statement will cause a move of reference in the memory.

    // So. Ownership.
    let x = String::from("asdf");

    // Another important thing. Scope is very important in Rust, and basically if a variable goes out of scope, it'll be disowned hence Dropped.
    // Drop is like free in C++

    {
        let _x = 1; // came into scope, x owns 1
    } // x is now out of scope, dropped (yeah,yeah the another x above is shadowed here but let's ignore that)

    // Ownership is transferred on function calls!
    reference_schematics_move_happens_taking_ownership(x); // move happens, old x is invalidated.

    // println!("Cannot access x anymore. It's ownership was passed {} to add.", x); // Oo-oo problem!: borrow of moved value: `x`

    let x3 = 1;
    let x4 = 2;
    add(x3, x4);

    println!("u32 is Copy, so no problem accessing them {x3}, {x4} as they were copied for the fn");

    // If a function call causes a move, but it returns the value that'll be ok (I bet there are 2 moves in the bg)
    // so you end up with owning the variable again. (I am very sure there is no such thing as owning it again.)
    // (In one of the first lessons in the tutorial shadowing was explained. I am quite sure what happens here:)

    let _x5 = 5;
    let _x5_ = id(_x5);
    println!("Print me {_x5}");

    // Is totally nothing new. In the tutorial it is mentioned as "returning the ownership" but I am pretty sure there is nothing like that.
    // _x5_ just gets assigned the value of the id function call expression and let _x5x_ = is nothing else just an assignment statement.
    // If you look at it from far you may say "returning ownership".

    // ok, continue: references, mutable references, nothing surprising:
    let r = String::from("asdf");
    ref_id(&r); // will not take ownership
                // nor will this:
    str_len(&r);

    let mut r2 = String::from("I'm mutable, I may be borrowed as mutable!");

    // aye mutable refs:
    mut_my_str(&mut r2);
    // one important thing: If you have a mutable ref to a value, no other references can exist to that!
    mut_my_str(&mut r2); // <- this works, but having 2 refs at the same time...

    // let r21: &mut String = &mut r2; // &mut String is actually inferred, no need to type it.
    // let r22 = &mut r2; // err: cannot borrow `r2` as mutable more than once at a time

    // println!("{} {}", r21, r22);

    // From the tutorial itself:
    // let mut s = String::from("hello");

    // let r1 = &s; // no problem
    // let r2 = &s; // no problem
    // let r3 = &mut s; // BIG PROBLEM

    // println!("{}, {}, and {}", r1, r2, r3);

    // One very important thing, and after all the above this should be expected, but still good to explicitly write it.
    // Note that a reference’s scope starts from where it is introduced and continues through the last time that reference is used.
    // For instance, this code will compile because the last usage of the immutable references, the println!,
    // occurs before the mutable reference is introduced:

    let mut s = String::from("hello");

    let r1 = &s; // no problem
    let r2 = &s; // no problem
    println!("{} and {}", r1, r2);
    // variables r1 and r2 will not be used after this point

    let r3 = &mut s; // no problem
    println!("{}", r3);

    // After reading the chapter, it still holds that Rust's tutorial is superb and useful.
}

fn reference_schematics_move_happens_taking_ownership(s: String) -> () {
    println!("Hello! You've given this string: {}", s);
}

fn add(a: u32, b: u32) -> u32 {
    a + b
}

fn id<T>(x: T) -> T {
    x
}

fn ref_id<T>(x: &T) -> &T {
    // "borrowing" the value
    x
}

fn str_len(x: &String) -> usize {
    x.len()
}

fn mut_my_str(x: &mut String) -> () {
    x.push_str("mutated")
}
