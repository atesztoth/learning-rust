// It's redefined on purpose, no worries!
#[derive(Debug)]
pub struct Rectangle {
    pub width: u32,
    pub height: u32,
}

// Let's have some simple example of the method syntax!
// No need for too much talk here, it's straightforward.
// Important thing: impl clause.
impl Rectangle {
    // note: &self is a syntactic sugar for: self: &Self
    // "need to use the & in front of the self shorthand to indicate that this method borrows the Self instance"
    fn area(&self) -> u32 {
        self.height * self.width
    }

    // &mut self ~> self: &mut Self
    fn incr_width(self: &mut Self) -> () {
        self.width += 1
    }

    pub fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

// Associated functions: they are associated with THE TYPE, not the instance!
// oh yeah, we can have multiple impl clauses for the same type, they'll behave
// like if they were one
impl Rectangle {
    // This is just an associated fn, first param is not of type Self!
    fn square(size: u32) -> Self {
        Self {
            height: size,
            width: size,
        }
    }
}

pub fn main() {
    let mut rect = Rectangle {
        height: 1,
        width: 2,
    };

    println!("{}", rect.area());

    rect.incr_width();

    println!("{}", rect.area());

    println!(
        "{}",
        rect.can_hold(&Rectangle {
            width: 1,
            height: 1
        })
    );

    let square = Rectangle::square(10);

    dbg!(square);
}
