// Opened a new file because the previous one was getting too large.
// This chapter comes:
// https://doc.rust-lang.org/book/ch13-01-closures.html#moving-captured-values-out-of-closures-and-the-fn-traits
// This seems to be very interesting at first glance.

// So in short, the closure can do many thing with captured values.
// Move it out from the closure, mutate it, neither move nor mutate,
// or just capture nothing.

// The way how closures handle the capture values affects what traits it implements.
// Also, traits are the way functions and structs can specify what closures they can use.

// Closures will automatically implement one, two, or all three of these Fn traits, in an additive fashion,
// depending on how the closure’s body handles the values:
// - FnOnce: every closure implements it. Closures that move out the capture value from its body only implement this one.
// (Can be only called once.)
// - FnMut: Applies to closures that mutate the captured value. These closures can be called more than once.
// - Fn: Applies to closures not moving values out of the body and not mutating anything, or not even
// capturing any value. These can be called many times without causing side effects. (Concurrency friendly)

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

pub fn main() {
    let mut list = [
        Rectangle {
            width: 10,
            height: 1,
        },
        Rectangle {
            width: 3,
            height: 5,
        },
        Rectangle {
            width: 7,
            height: 12,
        },
    ];

    // sort_by_key : FnMut
    // takes in a reference to a slice, and returns something that can be ordered
    // will sort ASC
    // Why the closure implements FnMut is because it can be called many times.
    // Why not just Fn trait? ... I don't know now, but it seems weird.
    // Who an earth would modify an element in a list while sorting using
    // basically a utility function made for sorting? ':D...
    list.sort_by_key(|r| r.width);
    println!("{:#?}", list);
}
