// implementing the example from the book here.

// Return and argument types are not required to be explicitly written out for closures, but for functions.
// This is because closures are not meant to be exposed in an API by any means unlike functions.

use std::fmt::{Debug, Display};
use std::thread;
use std::time::Duration;

// But of course, one can annotate types for closures:
fn example() {
    let _expensive_closure = |num: u32| -> u32 {
        println!("calculating slowly...");
        thread::sleep(Duration::from_secs(2));
        num
    };

    // fn  add_one_v1   (x: u32) -> u32 { x + 1 }
    // let add_one_v2 = |x: u32| -> u32 { x + 1 };
    // let add_one_v3 = |x|             { x + 1 };
    // let add_one_v4 = |x|               x + 1  ;

    // Some caveat:
    // let naive_id = |x| x;
    //
    // let s = naive_id(String::from("hello")); // type String -> String will be inferred here that sticks!
    // let n = naive_id(5); // This will not compile.
}

pub fn main() {
    let store = Inventory {
        shirts: vec![
            ShirtColor::Blue,
            ShirtColor::Blue,
            ShirtColor::Red,
            ShirtColor::Blue,
        ],
    };

    let user1_pref = ShirtColor::Blue;
    let giveaway1 = store.giveaway(&Some(user1_pref));

    println!("User with preference {:?} gets {:?}", user1_pref, giveaway1);

    let user2_pref = None;
    let giveaway2 = store.giveaway(&user2_pref);
    println!(
        "The user with preference {:?} gets {:?}",
        user2_pref, giveaway2
    );

    // Some words about capturing values:
    // Closures can capture values from their environment in three ways,
    // which directly map to the three ways a function can take a parameter:
    // borrowing immutably, borrowing mutably, and taking ownership.
    // The closure will decide which of these to use based on what the body of the function does with the captured values.
    fn example_only_borrows() {
        // There will be no problem with having multiple references to list,
        // because it is allowed to have multiple non mutating references to a value.
        println!("----");
        let list = vec![1, 2, 3];
        println!("Before defining closure: {:?}", list);

        let only_borrows = || println!("From closure: {:?}", list);

        println!("Before calling closure: {:?}", list);
        only_borrows();
        println!("After calling closure: {:?}", list);
        println!("----");
    }

    example_only_borrows();

    fn borrows_mutably() {
        // Note that there’s no longer a println! between the definition and the call of the borrows_mutably closure:
        // when borrows_mutably is defined, it captures a mutable reference to list.
        // We don’t use the closure again after the closure is called, so the mutable borrow ends.
        // Between the closure definition and the closure call,
        // an immutable borrow to print isn’t allowed because no other borrows are allowed when there’s a mutable borrow.
        // Try adding a println! there to see what error message you get!
        let mut list = vec![1, 2, 3];
        println!("Before defining closure: {:?}", list);

        let mut borrows_mutably = || list.push(7);

        // println!("cannot borrow `list` as immutable because it is also borrowed as mutable {:?}", list);

        borrows_mutably();
        // Here the mutable borrow ends
        // Ok this was vague. More precisely this happens:
        // We don’t use the closure again after the closure is called, so the mutable borrow ends.
        // Between the closure definition and the closure call, an immutable borrow to print isn’t allowed
        // because no other borrows are allowed when there’s a mutable borrow. Try adding a println! there to see what error message you get!

        println!("After calling closure: {:?}", list);
    }

    borrows_mutably();

    fn example_of_forcing_closure_to_take_ownership() {
        // If you want to force the closure to take ownership of the values it uses in the environment
        // even though the body of the closure doesn’t strictly need ownership,
        // you can use the move keyword before the parameter list.
        // !! This technique is mostly useful when passing a closure to a new thread to move the data so that it’s owned by the new thread. !!
        let _sample = move |val: &(dyn Debug + 'static)| {
            dbg!(val);
        };

        let list = vec!["A1", "B1", "C1"];
        // wow, wow, wow wild example appears, right away spawning a new thread and actually showing
        // an example mentioned above:
        thread::spawn(move || println!("HELLOO {:?}", list))
            .join()
            .unwrap();

        // In the example above it is very important that "list" is moved into the closure.
        // That is because the main thread or in a general case the spawner thread may end first.
        // Without the move keyword "only" an immutable borrow would happen from the side of the closure,
        // which does not prevent the owner thread to drop the variable hence invalidating the reference to list.
        // Now, if the value is moved the ownership is completely transferred to the closure, hence the thread,
        // so "list" will not be disturbed.
    }

    example_of_forcing_closure_to_take_ownership();
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum ShirtColor {
    Red,
    Blue,
}

struct Inventory {
    shirts: Vec<ShirtColor>,
}

impl Inventory {
    // Why not defining user_preference as a ref here btw? Why moving it in the function?
    fn giveaway(&self, user_preference: &Option<ShirtColor>) -> ShirtColor {
        // Closure syntax is already known by me, but | <argument list> |.
        // So || is an empty argument list.
        // Here the closure captured an immutable reference to self.
        // Oh, one should know about closures can capture values from its literal context.
        user_preference.unwrap_or_else(|| self.most_stocked())
    }

    // TODO: Rewrite this func to work on any element of the ShirtColor enum
    // That could be a nice exercise, this implementation is good for a tutorial but I want better.
    fn most_stocked(&self) -> ShirtColor {
        // O(n) counting how many we have of stuff
        let mut stock_blue = 0;
        let mut stock_red = 0;

        for color in &self.shirts {
            match color {
                ShirtColor::Red => stock_red += 1,
                ShirtColor::Blue => stock_blue += 1,
            }
        }

        if stock_blue > stock_red {
            ShirtColor::Blue
        } else {
            ShirtColor::Red
        }
    }
}
