#![allow(unused_variables)]

struct Point {
    x: i32,
    y: i32,
}

// From the book:
// Every time you've used a let statement like this you've been using patterns,
// although you might not have realized it!
// let PATTERN = EXPRESSION;
pub fn main() {
    example_if_let();
    example_while_let();
    example_for();

    // When using pattern matching, the destructor pattern must match the shape of the
    // type it is destructuring.
    // E.g.:
    let (x, y, z) = (1, 2, 3); // Will work
                               // let (y, z) = (1, 2, 3); // error
                               // let (_, y, z) = (1, 2, 3); // fixed
                               // let (.., y, z) = (1, 2, 3); // fixed
    println!("{},{},{}", x, y, z);

    example_functions();

    // Refutability:
    // Patterns come in two forms: refutable and irrefutable.
    // Patterns that will match for any possible value passed are irrefutable.
    // An example would be x in the statement let x = 5;
    // because x matches anything and therefore cannot fail to match.
    // Patterns that can fail to match for some possible value are refutable

    pattern_syntax();
}

// This is the example from the book,
// it is just here for exhaustiveness.
fn example_if_let() {
    // mixing if let with else is possible as shown in this example.
    // Important note: if let does not check exhaustively.
    // That should not come as a surprise since the goal of that expression is not that.
    fn main() {
        let favorite_color: Option<&str> = None;
        let is_tuesday = false;
        let age: Result<u8, _> = "34".parse();

        if let Some(color) = favorite_color {
            println!("Using your favorite color, {color}, as the background");
        } else if is_tuesday {
            println!("Tuesday is green day!");
        } else if let Ok(age) = age {
            if age > 30 {
                println!("Using purple as the background color");
            } else {
                println!("Using orange as the background color");
            }
        } else {
            println!("Using blue as the background color");
        }
    }
}

fn example_while_let() {
    let mut stack = Vec::new();

    // pushing to act more like a stack
    stack.push(1);
    stack.push(2);
    stack.push(3);

    // the let <expr> = <expr> pattern is usable with while also
    while let Some(top) = stack.pop() {
        println!("{}", top);
    }
}

fn example_for() {
    let v = vec!['a', 'b', 'c'];

    // This also destructures by the pattern that's supplied after for
    for (index, value) in v.iter().enumerate() {
        println!("{} is at index: {}", value, index);
    }
}

fn example_functions() {
    // When functions take arguments, those are also considered to be part of a pattern!
    // x is considered to be a pattern (refer to the main fn).
    fn foo(_x: i32) {
        // code goes here
    }

    fn print_coordinates(&(x, y): &(i32, i32)) {
        println!("Current location: ({}, {})", x, y);
    }

    print_coordinates(&(1i32, 2i32));
}

fn pattern_syntax() {
    // I created this section show Match Guards, multiple patterns gets mentioned.
    let x = 4;

    // Multiple patterns:
    match x {
        1 | 2 => println!("It's one or two!"),
        3 => println!("Three"),
        _ => println!("I know what you did last summer."),
    }

    // Matching a range:
    match x {
        1..=5 => println!("Range matching is awesome!"),
        _ => println!("I did not fell into your range. 😡"),
    }

    // The compiler checks that the range isn’t empty at compile time,
    // and because the only types for which Rust can tell if a range is empty or not
    // are char and numeric values, ranges are only allowed with numeric or char values.

    let x = 'c';

    match x {
        'a'..='j' => println!("early ASCII letter"),
        'k'..='z' => println!("late ASCII letter"),
        _ => println!("something else"),
    }

    example_destructuring();
    example_nested_destructuring();

    let ((feet, inches), Point { x, y }) = ((3, 10), Point { x: 3, y: -10 });

    // Next would be ignoring patterns, but that is already shown in this file I think.
    // It can be done using the hole "_", or the ".." operator.
    // Anyways:
    let _ = ((3, 10), Point { x: 3, y: -10 });
    // let .. = ((3, 10), Point { x: 3, y: -10 }); // error, invalid syntax (thank, God ... :D)
    let (_, Point { x, y }) = ((3, 10), Point { x: 3, y: -10 });
    let (.., Point { x, y }) = ((3, 10), Point { x: 3, y: -10 });
    let (a, ..) = ((3, 10), Point { x: 3, y: -10 });
    let (a, _) = ((3, 10), Point { x: 3, y: -10 });
    let ((x, y), _) = ((3, 10), Point { x: 3, y: -10 });

    // you get this...
    let numbers = (2, 4, 8, 16, 32);

    match numbers {
        (first, _, third, _, fifth) => {
            println!("Some numbers: {first}, {third}, {fifth}")
        }
    }

    // prefixing variables with "_" will make the compiler not complain if you don't use them,
    // this technique has also been used many times in these files.

    struct Point3D {
        x: i32,
        y: i32,
        z: i32,
    }

    let origin = Point3D { x: 0, y: 0, z: 0 };

    match origin {
        // Point3D { x } => println!("x is {}", x), // this would error
        // This means Aright man, just ignore anything after x.
        Point3D { x, .. } => println!("x is {}", x),
    }

    // using .. must be unambitious so if you have 2 elems before the third, you cannot just say
    // .., third... Would make no sense, hence:
    // let numbers = (2, 4, 8, 16, 32);
    // match numbers {
    //     (.., second, ..) => { // Error! Rust (any frankly, no one) can know how many elements the user intended to skip...
    //         println!("Some numbers: {}", second)
    //     },
    // }

    // Now this I will definitely need and want to use, it's very, very useful:
    // using extra conditionals

    let num = Some(4);

    match num {
        Some(x) if x % 2 == 0 => println!("The number {} is even", x),
        Some(x) => println!("The number {} is odd", x),
        None => (),
    }

    // !!!!!!!
    // The downside of this additional expressiveness is that
    // the compiler doesn't try to check for exhaustiveness when match guard expressions are involved.
    example_match_guard_for_avoiding_shadowing();

    example_at_bindings();
}

fn example_destructuring() {
    let p = Point { x: 1, y: 10 };
    // Now some examples:
    let Point { x, y } = p; // Note the original type had to be explicitly written
    let Point {
        x: point_x,
        y: point_y,
    } = p; // renaming right away works like a charm.

    println!("x: {}, y: {}", point_x, point_y);

    // Also works in match. (Why wouldn't it if you think about it... we are matching against patterns
    // that we saw we can define, so why would it be different in a match... Just thinking out loud.)
    match p {
        Point { x, y: 0 } => println!("On the x axis at {x}"),
        Point { x: 0, y } => println!("On the y axis at {y}"),
        Point { x, y } => {
            println!("On neither axis: ({x}, {y})");
        }
    }

    // I am not mentioning destructuring enums here, there was already a load of examples for that,
    // and at this point it should be trivial how it happens.
}

fn example_nested_destructuring() {
    // This should be also known by now, but this just looks good, let's have an example here!

    enum Color {
        Rgb(i32, i32, i32),
        Hsv(i32, i32, i32),
    }

    enum Message {
        Quit,
        Move { x: i32, y: i32 },
        Write(String),
        ChangeColor(Color),
    }

    let msg = Message::ChangeColor(Color::Hsv(0, 160, 255));

    match msg {
        Message::ChangeColor(Color::Rgb(r, g, b)) => {
            println!("Change color to red {r}, green {g}, and blue {b}");
        }
        Message::ChangeColor(Color::Hsv(h, s, v)) => {
            println!("Change color to hue {h}, saturation {s}, value {v}")
        }
        _ => (),
    }
}

fn example_match_guard_for_avoiding_shadowing() {
    let x = Some(5);
    let y = 10;

    match x {
        Some(50) => println!("Got 50"),
        // y is the outer y, nothing shadows it.
        Some(n) if n == y => println!("Matched, n = {n}"),
        _ => println!("Default case, x = {:?}", x),
    }

    println!("at the end: x = {:?}, y = {y}", x);

    // be aware of this syntax also:
    let x = 4;
    let y = false;

    match x {
        // this will match for 4,5,6 AND when y is true
        // so the precedence is like this:
        // (4 | 5 | 6) if y, not 4 | 5 | (6 if y)
        4 | 5 | 6 if y => println!("yes"),
        _ => println!("no"),
    }
}

fn example_at_bindings() {
    // The at operator @ lets us create a variable that holds a value at the same time
    // as we’re testing that value for a pattern match.
    // Using @ lets us test a value and save it in a variable within one pattern.

    enum Message {
        Hello { id: i32 },
    }

    let msg = Message::Hello { id: 5 };

    match msg {
        Message::Hello {
            id: id_variable @ 3..=7, // so id_variable will have a value between 3 and 7
        } => println!("Found an id in range: {}", id_variable),
        Message::Hello { id: 10..=12 } => {
            println!("Found an id in another range")
        }
        Message::Hello { id } => println!("Found some other id: {}", id),
    }
}
