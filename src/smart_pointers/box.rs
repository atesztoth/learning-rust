// The most straightforward smart pointer is a box, whose type is written Box<T>.
// Boxes allow you to store data on the heap rather than the stack.
// What remains on the stack is the pointer to the heap data.

// Most common use-cases:
// - When you have a type whose size can’t be known at compile time and you want to use a value of that type in a context that requires an exact size
// - When you have a large amount of data and you want to transfer ownership but ensure the data won’t be copied when you do so
// - When you want to own a value and you care only that it’s a type that implements a particular trait rather than being of a specific type

pub fn main() {
    // oookay, let's store some data on the heap!
    let x = Box::new(5);
    println!("Data from heap: {x}");
    // When de-allocation happens, data from the heap is removed, and also the memory address from the stack.

    // Recursive types with boxes:
    // The following quote from the book perfectly explains why you need to use Boxed types for recursive types.
    // "A value of recursive type can have another value of the same type as part of itself.
    // Recursive types pose an issue because at compile time Rust needs to know how much space a type takes up.
    // However, the nesting of values of recursive types could theoretically continue infinitely,
    // so Rust can’t know how much space the value needs. Because boxes have a known size,
    // we can enable recursive types by inserting a box in the recursive type definition."

    // Let's show two example. This first is in the book presenting the "cons list"
    // which I've implemented as an exercise when I started learning Haskell,
    // and interestingly I had absolutely no idea where it is coming from.
    // "A cons list is a data structure that comes from the Lisp programming language
    // and its dialects and is made up of nested pairs, and is the Lisp version of a linked list.
    // Its name comes from the cons function (short for “construct function”) in Lisp
    // that constructs a new pair from its two arguments.
    // By calling cons on a pair consisting of a value and another pair,
    // we can construct cons lists made up of recursive pairs."
    // Very cool info!

    // I like the implementation that the Rust book show's so let's do it here also.
    // I wanted to add generics to but that complicates things much preventing
    // the example to be focused on the point why it was created in the first place.
    // enum List {
    //     Cons(i32, List),
    //     Nil,
    // }

    // let list = List::Cons(1, List::Cons(2, List::Nil)); // oooh boy, problems!! PROBLEMS!!!
    // insert some indirection (e.g., a `Box`, `Rc`, or `&`) to break the cycle
    //
    //         Cons(i32, Box<List>),
    // In order to understand this properly first the way how Rust calculates
    // memory requirements for non-recursive types must be understood.

    // It's easy to understand. So:
    // enum Message {
    //     Quit,
    //     Move { x: i32, y: i32 },
    //     Write(String),
    //     ChangeColor(i32, i32, i32),
    // }
    // If you check how much space is needed to store e.g.: a Message.Quit, it's practically zero.
    // BUT Message.Move would need at least 2 x sizeof i32 (which is fix lol xd 32 bit = 4 byte).
    // So yeah... But when you have a recursive type it goes on indefinitely... Potentially infinite size!
    // SO! Why does "introducing some indirection" solve our problems?
    // Because the size of the pointer is known, and on the heap we can get as much memory as we want.
    // (Let's just say we have ideal circumstances, an infinite, calm and beautiful heap.)
    // Oh, one more info about pointer sizes: https://doc.rust-lang.org/std/primitive.usize.html .

    // WOW, quite a mouthful stuff! Let's do some exercise finally:

    #[derive(Debug)]
    enum List<T> {
        Cons(T, Box<List<T>>),
        Nil,
    }

    let list = List::Cons(1, Box::new(List::Cons(2, Box::new(List::Nil))));
    dbg!(list);

    // Let's do a binary tree:
    #[derive(Debug)]
    struct Tree<T> {
        value: T,
        left: Option<Box<Tree<T>>>,
        right: Option<Box<Tree<T>>>,
    }

    let tree: Tree<i32> = Tree {
        value: 10,
        left: Some(Box::new(Tree {
            value: 2,
            left: None,
            right: None,
        })),
        right: Some(Box::new(Tree {
            value: 12,
            left: None,
            right: Some(Box::new(Tree {
                value: 15,
                left: None,
                right: None,
            })),
        })),
    };
    dbg!(tree);

    // Hope there is some way to have Box::new calls generated automatically because this is very tedious.
}
