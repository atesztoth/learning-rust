// Using the Drop trait one can easily define what should happen when a value goes
// out of scope. Ideal place to release resources like files, network connections etc.

struct CustomSmartPointer {
    data: String,
}

impl Drop for CustomSmartPointer {
    fn drop(&mut self) {
        println!("Dropping CustomSmartPointer with data `{}`!", self.data);
    }
}

pub fn main() {
    println!("Drop trait.");

    let k = CustomSmartPointer {
        data: String::from("I'm the first, the special one."),
    };

    let _c = CustomSmartPointer {
        data: String::from("my stuff"),
    };
    let _d = CustomSmartPointer {
        data: String::from("other stuff"), // Gets dropped first. Dropping happens in reverse order.
                                           // I mean no. Technically this is the right order. This value went into scope later, so it's
                                           // natural to drop it first. I mean I don't know, but I feel like it's the natural way.
    };

    println!("CustomSmartPointer creation finished!");

    // Sometimes we want to drop values early, e.g. to make them release a lock.
    // Destructors are not to be called directly in Rust. Hence, Rust gives us a method to
    // achieve such shenanigans: std::mem::drop .

    // Let's drop the first smart pointer early:
    // k.drop(); // explicit destructor calls not allowed
    std::mem::drop(k); // Drops k before others as expected.

    // Also:
    // QUOTE TIME
    // "You also don’t have to worry about problems resulting from accidentally cleaning up values still in use:
    // the ownership system that makes sure references are always valid
    // also ensures that drop gets called only once when the value is no longer being used."
}
