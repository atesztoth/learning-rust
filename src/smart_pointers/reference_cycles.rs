use crate::smart_pointers::reference_cycles::List::{Cons, Nil};
use log::warn;
use std::cell::RefCell;
use std::rc::{Rc, Weak};

// Reference cycles are not a new thing for me, I've had to be on the lookout for them
// when I was programming in Swift day by day, so potentially I'll skip parts.
#[derive(Debug)]
enum List {
    // Note that now the second part, the tail of the list is
    // modifiable, not the actual value it holds!
    Cons(i32, RefCell<Rc<List>>),
    Nil,
}

impl List {
    fn tail(&self) -> Option<&RefCell<Rc<List>>> {
        match self {
            Cons(_, list) => Some(list),
            Nil => None,
        }
    }
}

pub fn main() {
    println!("Reference cycles.");

    // Now the goal is to create some code that produces a reference cycle.
    // What will happen is a list "a" will be created. Then "b", that points to "a",
    // then "a" will be modified to point to be, hence a cycle will arise.

    let a = Rc::new(Cons(5, RefCell::new(Rc::new(Nil)))); // this boilerplate tho...

    println!("initial rc count on a = {}", Rc::strong_count(&a));
    println!("next item in a = {:?}", a.tail());

    let b = Rc::new(Cons(10, RefCell::new(Rc::clone(&a))));

    println!("rc count on a after b creation = {}", Rc::strong_count(&a));
    println!("rc count on b after creation = {}", Rc::strong_count(&b));
    println!("next item in b = {:?}", b.tail());

    if let Some(_a_tail) = a.tail() {
        // now tail of "a" will point to "b",
        // cycle is born
        // uncomment it to create the cycle:
        // *_a_tail.borrow_mut() = Rc::clone(&b);
    }

    println!("b rc count after changing a = {}", Rc::strong_count(&b));
    println!("a rc count after changing a = {}", Rc::strong_count(&a));

    // This makes the code bananas, overflows the stack
    // because a -> b -> a -> b ......
    // println!("a next item = {:?}", a.tail());

    // When dropping of variables happen
    // ref count will decrease from 2 to 1 a then for b but still 1 remains...
    // You have a zombie list in the memory that won't be dropped.
    // This is perfectly explained in this paragraph:
    // https://doc.rust-lang.org/book/ch15-06-reference-cycles.html#creating-a-reference-cycle

    ///// ----
    example_tree_weak_ref();
    example_tree_weak_ref_counts();
}

// Now let's introduce Weak references!
// So strong references are the way to share ownership of a Rc<T> instance,
// and one can downgrade this "ownership" by calling Rc::downgrade to make it just
// expressing a reference (in the casual meaning of the word).

// Calling Rc::downgrade will return a smart pointer of type Weak<T>,
// also, it increases "weak_count" instead of "strong_count" in Rc<T>.

// Again, weak reference count does not affect if a resource can be cleaned up or not.

// Now, a problem arises. Since weak refs doesn't prevent a resource to be dropped,
// it must be ensured that it is still referencing something at the time of usage.
// That can be done by calling "upgrade" of Weak<T>. That returns an Option<Rc<T>>.

// Now, let's do an example with a tree.
#[derive(Debug)]
struct Node {
    value: i32,
    // added later:
    parent: RefCell<Weak<Node>>,
    children: RefCell<Vec<Rc<Node>>>,
}

// We want a node to own the children of it,
// and we want to share that ownership with variables so Nodes can be accessed directly.
fn example_tree_weak_ref() {
    let leaf = Rc::new(Node {
        value: 3,
        parent: RefCell::new(Weak::new()),
        children: RefCell::new(vec![]),
    });

    let branch = Rc::new(Node {
        value: 5,
        parent: RefCell::new(Weak::new()),
        children: RefCell::new(vec![Rc::clone(&leaf)]),
        // The node in leaf now has 2 owners!
    });

    // Now adding the parent prop to the struct so leafs know it's parents.
    // Problem: if the parent was a Rc, then parent -> children -> parent...
    // cycle.
    // But the parent should own its children, if the parent is dropped,
    // the children should also go. But a child not necessarily has a parent, so
    // that children is a parent. Let's make the parent ref a weak ref, and if a child is a child,
    // no cycles will arise!

    *leaf.parent.borrow_mut() = Rc::downgrade(&branch);

    println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());
    // Note: no infinite output! That signals the lack of a cycle.
}

fn example_tree_weak_ref_counts() {
    println!("----------------------------");
    println!("Example for weak ref counts");

    let leaf = Rc::new(Node {
        value: 3,
        parent: RefCell::new(Weak::new()),
        children: RefCell::new(vec![]),
    });

    println!(
        "leaf strong = {}, weak = {}",
        Rc::strong_count(&leaf),
        Rc::weak_count(&leaf),
    );

    println!("------------");

    {
        let branch = Rc::new(Node {
            value: 5,
            parent: RefCell::new(Weak::new()),
            children: RefCell::new(vec![Rc::clone(&leaf)]),
        });

        *leaf.parent.borrow_mut() = Rc::downgrade(&branch);

        println!(
            "branch strong = {}, weak = {}",
            Rc::strong_count(&branch),
            Rc::weak_count(&branch),
        );

        println!(
            "leaf strong = {}, weak = {}",
            Rc::strong_count(&leaf),
            Rc::weak_count(&leaf),
        );

        println!("------------");
    }

    println!("branch goes out of scope, then:");
    println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());
    println!(
        "leaf strong = {}, weak = {}",
        Rc::strong_count(&leaf),
        Rc::weak_count(&leaf),
    );

    // Perfect, leaf has only 1 strong ref pointing to it, that's the first variable.
}
