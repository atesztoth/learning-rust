use std::ops::{Deref, DerefMut};

pub fn main() {
    println!("Deref trait");
    // The Deref trait requires implementing the * operator. I wanted to say this previously
    // in smart_pointers.rs file, but it was too early. In short,
    // when creating a custom smart pointer implementing this operator you can specify how
    // precisely it should return the pointed value, maybe add some custom magic?

    // "A regular reference is a type of pointer, and one way to think of a pointer is as an arrow to a value stored somewhere else."
    // Why, why, why this analogy again? This is the most commonly used welcome phrase for pointers AND this confuses people very much...
    // https://www.youtube.com/watch?v=DTxHyVn0ODg . I remember, anyone who first met pointers as was told it's an arrow
    // RIGHT AWAY they had a bad intuition building in their minds. Suddenly exams became extremely hard to pass.
    // It is just a damn address!

    // Anyway, dereference operator in action:
    let x = 5;
    let y = &x;

    assert_eq!(5, x);
    assert_eq!(5, *y);
    // assert_eq!(5, y);
    //    |     ^^^^^^^^^^^^^^^^ no implementation for `{integer} == &{integer}`
    // Just as expected.

    // ok now show that box also has an implementation for the deref operator:
    let x = 5;
    let y = Box::new(5);

    assert_eq!(5, x);
    assert_eq!(5, *y);
    // Super!

    // This is a tuple struct with one element!
    struct MyBox<T>(T);

    impl<T> MyBox<T> {
        fn new(x: T) -> MyBox<T> {
            MyBox(x)
        }
    }

    // let hello = MyBox::new(5);
    // assert_eq!(5, *hello); // type `MyBox<{integer}>` cannot be dereferenced

    // oooookay, let's do a dedicated implementation of the Deref trait for MyBox!
    impl<T> Deref for MyBox<T> {
        type Target = T; // associated type is T, don't worry about it, it's like a generic type
                         // (actually associates this type with the current instance ... anyways, later)

        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }

    let my_box = MyBox::new(5);
    dbg!(*my_box);
    assert_eq!(5, *my_box);
    // *my_box is a syntactic sugar for: *(y.deref())

    // A very important comment from the book:
    // "The reason the deref method returns a reference to a value,
    // and that the plain dereference outside the parentheses in *(y.deref()) is still necessary,
    // is to do with the ownership system.
    // If the deref method returned the value directly instead of a reference to the value,
    // the value would be moved out of self.
    // We don’t want to take ownership of the inner value inside MyBox<T> in this case
    // or in most cases where we use the dereference operator."

    // There is a phenomena called "deref coercion" is Rust.
    // It is a conversion from a reference type that implements the Deref trait to another reference type.
    // E.g.: String -> &str. String implements the Deref trait such that it produces a &str.
    // This is a convenience feature because e.g. when you pass in a String for a function that
    // takes a &str (string slice) then it'll be automatically converted.

    // Unintentionally wrote the same example as in the book.
    let greeter = |s: &str| -> () { println!("Hello {}!", s) };

    let string_example = String::from("World");
    greeter(&string_example); // &String -> &str , deref coercion
    greeter(&*&*&*string_example); // ref-deref-ref-deref.....

    let m = MyBox::new(String::from("Rust"));
    greeter(&m); // Deref coercion! In a finite number of calls to deref we can get to the final type.
                 // This is one automatically. (Feature of deref coercion.)
                 // These calls to Deref::deref is done automatically and in compile time so
                 // there is no runtime penalty for it.

    // If Rust didn't have deref coercion:
    let m = MyBox::new(String::from("Rust"));
    greeter(&(*m)[..]); // deref MyBox, get a String type -> Take a slice of that string (whole string), then return a reference to it.
                        // This is a very important example imo. If one wants to learn something well, understanding the underlying concepts is
                        // a necessity. Basically nothing else matters imo.

    // For mutable references the DerefMut trait can be used.
    // Let's write it quickly:
    impl<T> DerefMut for MyBox<T> {
        fn deref_mut(&mut self) -> &mut Self::Target {
            &mut self.0
        }
    }

    // Rust does deref coercion when it finds types and trait implementations in three cases:

    // - From &T to &U when T: Deref<Target=U> // So no mutability changes when not mut -> not mut
    // - From &mut T to &mut U when T: DerefMut<Target=U> // mut -> mut
    // - From &mut T to &U when T: Deref<Target=U> // mut -> not mut depending on type of the target

    // QUOTE:
    // The first two cases are the same as each other except that the second implements mutability.
    // The first case states that if you have a &T, and T implements Deref to some type U, you can get a &U transparently.
    // The second case states that the same deref coercion happens for mutable references.
    //
    // The third case is trickier: Rust will also coerce a mutable reference to an immutable one.
    // But the reverse is not possible: immutable references will never coerce to mutable references.
    // Because of the borrowing rules, if you have a mutable reference,
    // that mutable reference must be the only reference to that data (otherwise, the program wouldn’t compile).
    // Converting one mutable reference to one immutable reference will never break the borrowing rules.
    // Converting an immutable reference to a mutable reference
    // would require that the initial immutable reference is the only immutable reference to that data,
    // but the borrowing rules don’t guarantee that.
    // Therefore, Rust can’t make the assumption that converting an immutable reference to a mutable reference is possible.
}
