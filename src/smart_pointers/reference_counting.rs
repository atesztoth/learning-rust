#![allow(unused_variables)]

// Reference counting  has been mentioned in previous files. (smart_pointers.rs I guess).
// It can be very useful in cases where a resource is owned by many other resources.
// E.g.: while representing a graph multiple edges can own a node.
use crate::smart_pointers::reference_counting::List::{Cons, Nil};
use std::rc::Rc;

#[derive(Debug)]
enum List {
    Cons(i32, Rc<List>),
    Nil,
}

pub fn main() {
    println!("Reference counting");

    // What happens here is by using Rc API of Rust we can create a smart pointer that is reference counted.
    // Now, a will have a reference and ownership of the list constructed in it.
    let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
    let b = Cons(3, Rc::clone(&a)); // b has the ownership of a and a reference counter is increased to two (refs to a)
    let c = Cons(4, Rc::clone(&a)); // count increased to 3, c also owns a

    // Again with some helper text:
    let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
    println!("count after creating a = {}", Rc::strong_count(&a)); // Strong count returns the number of strong references to a resource
                                                                   // Yes, Rust also has weak references (it will be mentioned later, but the point is:
                                                                   // these are references that even is exists, the resource still can be dropped. They are for preventing
                                                                   // memory leaks.)
    let b = Cons(3, Rc::clone(&a));
    println!("count after creating b = {}", Rc::strong_count(&a));
    {
        let c = Cons(4, Rc::clone(&a));
        println!("count after creating c = {}", Rc::strong_count(&a));
    }
    println!("count after c goes out of scope = {}", Rc::strong_count(&a));

    // RC provides read-only access. This is in order to prevent race conditions etc.
    // that could arise from having multiple mutable refs for a particular resource.
    // "Via immutable references, Rc<T> allows you to share data between multiple parts of your program for reading only."
    // Tho, one can use the interior mutability pattern and Rc to be able to mutate the data through Rc.
}
