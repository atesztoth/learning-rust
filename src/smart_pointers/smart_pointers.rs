pub fn main() {
    println!("Hello from Smart Pointers!");

    // Pointers could be a topic for a day-long talk, but let's just state te most important things.
    // For some reason some devs think pointers are creatures of magic, and possess
    // some kind of unexplainable capabilities. At first, they are though of some kind of special
    // construct in the memory that are handled differently and act differently then any other construct
    // in a language. [...]
    // As a matter of fact they are nothing else just memory addresses. They're named as pointers
    // because, well, they can be used to point to a location in memory, but at the base level they
    // are just memory addresses, nothing else, not pointing anywhere. This is actually very rarely told
    // by teachers when they introduce pointers to people. Sorry, already talked too much.
    //
    // --
    // I wanted to add this for a long time now:
    // At that point, I had no chance of knowing about it, BUT NOW COMES references and raw pointers to DSTs
    // and kick the door! Welcome FAT POINTERS. They are actually pointers with some extra metadata that makes the DST complete,
    // e.g. the address and the length.
    // ---

    // So, references are also pointers in Rust (surprised?). They use the "&" operator and
    // then the value next to them. E.g.:
    let x = 5;
    let x_ref = &x;
    println!("Address of x: {:p}", x_ref);
    // See? No magic. (Just passing {}, x_ref to println would print out the value (5)
    // because this "template" string is parsed by whatever code the println! macro runs,
    // but that is not important. The point is &x is an address!)

    // Memory management errors result from using pointers are notorious. To give a helping hand
    // Smart Pointer was born. Now this is more like the "magic" I was talking about at the beginning,
    // they aren't just memory addresses, rather memory addresses with "some salt" (metadata stored with them).
    // So yeah they take up a bit more space in memory BUT gives convenience and safety in return.

    // Smart pointers (opposed to refs) usually own the data, not just reference it.
    // String and Vec are also count as smart pointers since they own the memory and allow manipulating
    // it. The most important magic spell of String is it always ensures the data it manages
    // when parsed as UTF-8 characters will always be valid. What a nice abstraction!
    // (Who thinks I am joking or making fun of String by saying "What a nice abstraction" first should know
    // NO, I would never do such thing. I know proper string manipulation is not a calming task
    // for a weekend BBQ...)

    // Implementation details:
    // Smart pointers are usually implemented as structs, but it is required that they
    // implement the Deref, and Drop traits.
    // "The Deref trait allows an instance of the smart pointer struct to behave like a reference
    // so you can write your code to work with either references or smart pointers."
    // "The Drop trait allows you to customize the code
    // that’s run when an instance of the smart pointer goes out of scope."

    // Some smart pointers (box came of in my previous codes in this repo when I could not calm myself
    // and had to write something more advanced than my knowledge of Rust allowed at that time. :D )
    // Box<T> for allocating values on the heap
    // Rc<T>, a reference counting type that enables multiple ownership
    // Ref<T> and RefMut<T>, accessed through RefCell<T>, a type that enforces the borrowing rules at runtime instead of compile time

    // Reference counting is a practice when any references to a resource is literally counted like 1,2,3....
    // and when a reference is no longer used and dropped the counter is decremented. When it reaches
    // 0 the resource can be safely removed.
    // Now eagle-eyed ones may already see if you have a reference cycle, e.g.: A -> B -> A then
    // and no other references are there to these objects then you have 2 zombie objects whose cannot
    // be deleted because they have references to each other. It's a little memory leak.

    // Oh, and in the book they mentioned interior mutability. So this is a pattern when you
    // basically have a wrapper type that hold something inside, some resource.
    // Now this type is immutable, but you still provide some API to mutate the resource it holds.
    // Hence the name: interior mutability.
}
