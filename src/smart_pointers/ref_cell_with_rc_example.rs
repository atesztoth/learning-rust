#[derive(Debug)]
enum List {
    // Ok, so what's going on here?
    // The Cons ctor of the List contains a smart pointer that allows reference counting over
    // the smart pointer RefCell that manages borrows to the underlying i32 typed value.
    // This way the topmost Rc pointer can provide the functionality of giving ownership
    // and access  for multiple consumers to the underlying value of which borrowing status is managed
    // by a single RefCell pointer. Hence, safety is still granted.
    Cons(Rc<RefCell<i32>>, Rc<List>),
    Nil,
}

use crate::smart_pointers::ref_cell_with_rc_example::List::{Cons, Nil};
use std::cell::RefCell;
use std::rc::Rc;

pub fn main() {
    let value = Rc::new(RefCell::new(5));

    // Giving access to the same value for the list via Rc::clone();
    // And what are we cloning? A Rc<RefCell<i32>>!
    let a = Rc::new(Cons(Rc::clone(&value), Rc::new(Nil)));

    let b = Cons(Rc::new(RefCell::new(3)), Rc::clone(&a));
    let c = Cons(Rc::new(RefCell::new(4)), Rc::clone(&a));

    println!("a before = {:?}", a);
    println!("b before = {:?}", b);
    println!("c before = {:?}", c);

    println!("Increasing the value all holds refs to.");
    *value.borrow_mut() += 10;

    println!("a after = {:?}", a);
    println!("b after = {:?}", b);
    println!("c after = {:?}", c);
}
