pub fn main() {
    println!("RefCell<T> and the Interior Mutability Pattern");

    // This is he first unsafe code in this repo I am writing.
    // Writing any unsafe stuff in Rust is like you tell the compiler "Hey Bro no worries,
    // I'll take care of checking the borrowing rules."

    // RefCell represents single ownership of the resource, just like Box.
    // But there is a difference. Using Box, the borrowing rules are enforced at compile time.
    // Using RefCell, these rules are enforced at RUNTIME, this means if they aren't respected,
    // the program will panic.

    // Why would anyone do runtime borrowing rule checks then?
    // It is because it allows the discovery of some memory-safe situations that
    // at compile time is undetectable.
    // There are some properties of programs that cannot be detected by a static analyzer like
    // the Rust compiler, e.g.: the halting problem, which is NP-hard.

    // The book says the Rust compiler is conservative. What it means under it is since it is not
    // possible to discover every aspect of a program, if the Rust compiler cannot be sure if
    // the program respects the ownership rules it'll reject a correct program.
    // Of course, it is rather desired to reject a correct program then to ever accept an incorrect one...
    // In these cases should we take over.
    // "The RefCell<T> type is useful when you’re sure your code follows the borrowing rules but the compiler is unable to understand and guarantee that."

    // "Similar to Rc<T>, RefCell<T> is only for use in single-threaded scenarios
    // and will give you a compile-time error if you try using it in a multithreaded context."
    // Wait what? Bye-bye for interior mutability in multithreaded contexts? Nope. Later the solution will be shown.

    // let x = 5;

    // Now let's do some nasty:
    // let y = &mut x; // cannot borrow as mutable (x was declared as immutable!)
    // So to recall, interior mutability works by having an outer type that is in fact immutable,
    // but the value inside will be mutated.
    // Useful for scenarios when something some value should be immutable but would be cool if it could present
    // some methods that can do the mutation internally. Even for simple constructions.
}

// This is an example from the book.
// The point is to test if this code works correctly.
// Key points: implementing of an object that conforms to Messenger trait is easy,
// also that object can be monitored to see if the correct calls arrive.
// Then we can create a new instance of the LimitTracker, set the value,
// call set_value and call it a day... Is it that easy? No.
pub trait Messenger {
    fn send(&self, msg: &str);
}

pub struct LimitTracker<'a, T: Messenger> {
    messenger: &'a T,
    value: usize,
    max: usize,
}

impl<'a, T> LimitTracker<'a, T>
where
    T: Messenger,
{
    pub fn new(messenger: &'a T, max: usize) -> LimitTracker<'a, T> {
        LimitTracker {
            messenger,
            value: 0,
            max,
        }
    }

    pub fn set_value(&mut self, value: usize) {
        self.value = value;

        let percentage_of_max = self.value as f64 / self.max as f64;

        if percentage_of_max >= 1.0 {
            self.messenger.send("Error: You are over your quota!");
        } else if percentage_of_max >= 0.9 {
            self.messenger
                .send("Urgent warning: You've used up over 90% of your quota!");
        } else if percentage_of_max >= 0.75 {
            self.messenger
                .send("Warning: You've used up over 75% of your quota!");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::cell::RefCell;

    struct MockMessenger {
        sent_messages: RefCell<Vec<String>>,
    }

    impl MockMessenger {
        pub fn new() -> MockMessenger {
            // MockMessenger { sent_messages: vec![] }
            MockMessenger {
                sent_messages: RefCell::new(vec![]),
            }
        }
    }

    impl Messenger for MockMessenger {
        fn send(&self, msg: &str) {
            // Note a thing: not using the reference, rather creating a new string form it.
            // self.sent_messages.push(String::from(msg)) // Cannot borrow immutable local variable `self.sent_messages` as mutable
            // self.sent_messages.push(String::from(msg)) // Cannot borrow immutable local variable `self.sent_messages` as mutable
            //     |             ^^^^^^^^^^^^^^^^^^ `self` is a `&` reference, so the data it refers to cannot be borrowed as mutable
            // Borrowing it locally? Ahh... Nope. Don't really want to enable mutability over the object.
            // If &mut self was used, it would not match the expected signature. But anyway, that error
            // is a side effect of the main problem. Don't even want to make the object mutable in this method.
            // So, now let's make sent_messages a RefCell!
            self.sent_messages.borrow_mut().push(String::from(msg));

            // let mut fst_borrow = self.sent_messages.borrow_mut();
            // let mut snd_borrow = self.sent_messages.borrow_mut(); // already borrowed: BorrowMutError, borrowing rules had been hurt.

            // Super! So RefCell really is a wrapper around a value that manages borrowing rules.
        }
    }

    #[test]
    fn it_sends_an_over_75_percent_warning_message() {
        let mock_messenger = MockMessenger::new();
        let mut limit_tracker = LimitTracker::new(&mock_messenger, 100);

        limit_tracker.set_value(80);

        assert_eq!(mock_messenger.sent_messages.borrow().len(), 1);
    }
}

// Under the hood...
// Under the hood RefCell borrow() works by returning a smart pointer of type Ref<T>,
// and borrow_mut() works by returning a smart pointer of type RefMut<T>.
// Both of these types (Ref, RefMut) implements the Deref trait, can be used as simple references.
// I now reference here smart_pointers.rs and deref.rs immutably. :P

// Now, what RefCell<T> does is literally doing at runtime what the compiler does at static time.
// That is keeping track of how many of the particular types of references there are for a resource.
// Calling borrow() increases immutable ref count by one, analogous to borrow_mut().
// Ref<T> goes out of scope, immutable ref count decreases, analogous to RefMut<T>.
// Should one hurt the rules, a runtime panic will happen.

// Because of the runtime logic required to keep track of the borrowing rules, a small
// runtime penalty has to be paid.

// Creating a wild combination: RC with RefCell!
// Having multiple owners and the ability of mutation!

// HOHO, SPOILER
// Mutex<T> is the thread-safe version of RefCell<T> and we’ll discuss it later!
