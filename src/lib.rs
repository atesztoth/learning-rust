//! # Learn Rust
//!
//! `learn_rust` is a project where I test stuff out while reading
//! the Rust book.

pub fn adder(n: i32) -> i32 {
    1 + n
}
