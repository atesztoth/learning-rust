#![allow(unused_variables)]

pub fn structs() -> () {
    struct User {
        active: bool,
        username: String,
        email: String,
        sign_in_count: u64,
    }

    // Either the whole struct is mutable, or it isn't.
    let mut user = User {
        active: true,
        username: String::from("atesztoth"),
        email: String::from("a@a.com"),
        sign_in_count: 1,
    };

    // Property accessing: dot
    user.email = String::from("more-real-email@example.com");

    // "Struct update syntax"
    // Note that the struct update syntax uses = like an assignment;
    // this is because it moves the data, just as we saw in the
    // “Variables and Data Interacting with Move” section.
    // In this example, we can no longer use user1 as a whole after creating user2
    // because the String in the username field of user1 was moved into user2.
    // If we had given user2 new String values for both email and username,
    // and thus only used the active and sign_in_count values from user1,
    // then user1 would still be valid after creating user2.
    let user2 = User {
        email: String::from("t@t.hu"),
        ..user
    };

    // Tuple structs (giving name of related data without naming the props themselves)
    struct Color(i32, i32, i32);
    struct Point(i32, i32, i32);

    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);

    // Unit-Like Structs (no fields)
    // Useful when only need a trait, but the type has no values.

    struct AlwaysEqual;

    let subject = AlwaysEqual;
    // Eq: It is definable that any "AlwaysEqual" is always equal to
    // any other type.
}
