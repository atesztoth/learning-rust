// The term macro refers to a family of features in Rust: declarative macros with macro_rules! and three kinds of procedural macros:
//
//     Custom #[derive] macros that specify code added with the derive attribute used on structs and enums
//     Attribute-like macros that define custom attributes usable on any item
//     Function-like macros that look like function calls but operate on the tokens specified as their argument

// The derive attribute generates traits.

// Macros are interpreted before compilation, and processed differently than e.g. C macros, here
// operator precedence is actually kept.

// You must define macros or bring them into scope before you call them in a file,
// as opposed to functions you can define anywhere and call anywhere.

use hello_macro::HelloMacro;
use hello_macro_derive::HelloMacro;

pub fn main() {
    example_declarative_macros();
    example_procedural_macros();

    quiz_example();
}

// *** Declarative Macros with macro_rules! for General Metaprogramming
fn example_declarative_macros() {
    // The most widely used form of macros in Rust is the declarative macro.
    // These are also sometimes referred to as “macros by example,” “macro_rules! macros,” or just plain “macros.”

    // So what this macro does is something like match in control flow.
    // There, given an expression, the result value is compared to patterns, and then the
    // related code is run.

    // Here, the previously mentioned value would be Rust code,
    // that is matched against structure of the code,
    // and then replaced with the code related to that matching structure pattern.

    // Let's see an example using the macro_rules construct.
    // Re-writing the example from the book here. I know this is some work for robots,
    // but let's try developing some muscle memory!

    #[allow(non_local_definitions)]
    #[macro_export]
    macro_rules! vec {
        ( $( $x:expr ),* ) => {
            {
                let mut v = Vec::new();
                $(
                  v.push($x);
                )*
                v
            }
        }
    }

    // The #[macro_export] annotation indicates that this macro should be made available whenever
    // the crate in which the macro is defined is brought into scope.
    // Without this annotation, the macro can’t be brought into scope.

    // macro_rules! <macro name> {
    //  <pattern definition for the arm> => { { <code for the arm> } }, // +
    // }
    // explanation can be found here: https://rust-book.cs.brown.edu/ch19-06-macros.html#declarative-macros-with-macro_rules-for-general-metaprogramming
    // In short: $ is used for macro variable declaration, "expr" means it's an expression,
    // "$x:expr" pattern matches onto the "(expression)," and puts the matches into $x,
    // then in the body $( ... )* is called as many times as many matches happened.

    // Book on macros: https://veykril.github.io/tlborm/

    // How I feel tho? ... The more one learn, the more one realises one knows nothing. ... Just as always.

    // Full syntax: https://doc.rust-lang.org/reference/macros-by-example.html

    // Patterns are matched against Rust code.
    // Declarative macros can match against expressions (expr), types (ty), and even entire items (item).
}

// *** Procedural Macros for Generating Code from Attributes

// Procedural macros are kind of macros that accept code as argument and work on that code,
// produce some code as output from that code, not matching and replace them like a declarative macro would do.
// The three kinds of procedural macros are custom derive, attribute-like, and function-like,
// and all work in a similar fashion.

// When creating procedural macros, the definitions must reside in their own crate with a special crate type.
// This is for complex technical reasons that we (the Rust team) hope to eliminate in the future.

#[derive(HelloMacro)]
struct Pancakes;

fn example_procedural_macros() {
    Pancakes::hello_macro();
}

// *** Attribute-like macros

// Attribute-like macros are similar to custom derive macros,
// but instead of generating code for the derive attribute,
// they allow you to create new attributes.
//
// They’re also more flexible: derive only works for structs and enums;
// attributes can be applied to other items as well, such as functions.
//
// This is the kind used for route annotation (web).
// e.g.: #[route(GET, "/")]

// They need a crate with "proc-macro" crate type.
// [lib]
// proc-macro = true
// (Just like hello_macro_derive)

// #[proc_macro_attribute]
// pub fn route(attr: TokenStream, item: TokenStream) -> TokenStream {
//     todo!()
// }

// *** Function-like macros

// Function-like macros define macros that look like function calls.
// Similarly to macro_rules! macros, they’re more flexible than functions;
// for example, they can take an unknown number of arguments.

// Function-like macros take a TokenStream parameter and their definition manipulates that TokenStream
// using Rust code as the other two types of procedural macros do.

// e.g.:
// let sql = sql!(SELECT * FROM posts WHERE id=1);
// #[proc_macro]
// pub fn sql(input: TokenStream) -> TokenStream { ... }

macro_rules! manylet {
    ( $( $i:ident ),* = $e:expr ) => {
        $(
            #[allow(unused_mut)]
            let mut $i = $e;
        )*
    }
}

fn quiz_example() {
    // let mut s = String::from("A");
    let s = String::from("A");
    manylet!(x, y = s.clone()); // Original example did not compile because s was moved into the macro.
                                // wow, even here the borrow checker runs...
    x.push_str("B");
    println!("{x}{y}");
}

// Hmm:
// Q: Which of the following are valid reasons for implementing
// a macro as a procedural macro instead of a declarative macro?

// Answers:
// - You want to integrate with Rust's derive system
// - Your macro requires nontrivial analysis of the macro user's syntax

// Procedural macros are the only way to create a custom derive.
// Procedural macros are also useful when
// you need code to analyze the macro user's syntax —
// declarative macros only permit shuffling around the input, not e.g. computing its size.
//
// Declarative macros can generate variable-length sequences of code,
// and can wrap/produce items and not just expressions.
