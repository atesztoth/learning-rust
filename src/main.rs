//
#![allow(unused_imports)]
// Packages: A Cargo feature that lets you build, test, and share crates
// Crates: A tree of modules that produces a library or executable
// Modules and use: Let you control the organization, scope, and privacy of paths
// Paths: A way of naming an item, such as a struct, function, or module

// This main.rs is the entry point for the binary crate,
// and we treat separate rs files as modules using mod.

// That turns off dead code warnings globally.
#![allow(dead_code)]

// That turns off unused variables warnings globally.
// #![allow(unused_variables)]

mod collection_basics;
mod control_flow_and_others;
mod data_types;
mod loops;
mod number_guesser;
mod ownership;
mod playground;
mod slices;
mod structs;
mod structs2;
mod vars;

mod collections;
mod enums;
mod errors;
mod generics;
mod structs3;
mod testing;

mod closures;
mod iterators;

mod cargo;
mod concurrency;
mod smart_pointers;

mod oop;
mod pattern_matching;

mod advanced_features;

mod macros;

mod index_example;

use advanced_features::{
    advanced_fn_closures, advanced_traits, advanced_types, fully_qualified_syntax, unsafe_traits,
    unsafe_waters,
};
use cargo::cargo as cargo_main;
use closures::{closures as closures_first, closures_reloaded};
use collections::{hash_maps, strings, vectors};
use concurrency::{shared_state_concurrency, thread_communication, threads};
use errors::{guess_game2, unrecoverable_errors};
use generics::{examples, mad_structs_with_refs, references_with_lifetimes, traits_consumer};
use iterators::iterators as iterators_main;
use macros::macros as mod_macros;
use oop::{oop as oop_module, state_pattern, state_pattern_2};
use pattern_matching::pattern_matching as pattern_mod;
use smart_pointers::{
    deref_trait, drop_trait as drop_the_bass, r#box as box_main, ref_cell,
    ref_cell_with_rc_example, reference_counting, reference_cycles,
    smart_pointers as smart_pointers_main,
};
use testing::testing as testing_module;

fn main() {
    // vectors::main();
    // strings::main();
    // hash_maps::main();
    //
    // unrecoverable_errors::main().unwrap();
    // guess_game2::main();
    //
    // examples::main();
    // traits_consumer::main();
    // references_with_lifetimes::main();
    // testing_module::main();
    // closures_first::main();
    // closures_reloaded::main();
    // iterators_main::main();
    // cargo_main::main();
    // smart_pointers_main::main();
    // box_main::main();
    // deref_trait::main();
    // drop_the_bass::main();
    // reference_counting::main();
    // ref_cell::main();
    // ref_cell_with_rc_example::main();
    // reference_cycles::main();
    // threads::main();
    // thread_communication::main();
    // shared_state_concurrency::main();
    // oop_module::main();
    // state_pattern::main();
    // state_pattern_2::main();
    // pattern_mod::main();
    // unsafe_waters::main();
    // unsafe_traits::main();
    // advanced_traits::main();
    // fully_qualified_syntax::main();
    // advanced_types::main();
    // advanced_fn_closures::main();
    // mod_macros::main();
    index_example::main();
}
