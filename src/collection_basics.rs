pub fn main() -> () {
    let coll: [&str; 3] = ["a", "b", "c"];

    for element in coll {
        println!("Element: {}", element);
    }
}
