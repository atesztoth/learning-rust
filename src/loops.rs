pub fn main() -> () {
    // loops: loop, while, for

    let mut y = 1;
    let x = loop {
        y = y + 1;

        if y > 5 {
            // Returning a value from a loop:
            break y;
        }
    };

    // loops can be labelled
    let mut z = 0;
    'counter_loop: loop {
        z = z + 1;

        if z >= 10 {
            break 'counter_loop;
            // Break and return
            // break 'counter_loop z;
        }
    }

    println!("Value from the loop: {x}");

    for number in 1..10 {
        println!("Ranges are cool {}", number);
    }

    for number in (1..10).rev() {
        println!("Ranges can be reversed {}", number);
    }
}
