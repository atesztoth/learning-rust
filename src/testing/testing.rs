// Testing nyeh... We all know cool guys test on production,
// wise guys rather write some tests even if they're cool.

pub fn main() {
    println!("Testing")
}

#[cfg(test)]
mod tests {
    use crate::errors::guess_game2::Guess;
    use crate::structs3::Rectangle;

    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }

    #[test]
    fn failing_test() {
        // panic!("I had to write a test!")
        assert_eq!(1, 1);
    }

    #[test]
    fn larger_can_hold_smaller() {
        let larger = Rectangle {
            width: 8,
            height: 7,
        };
        let smaller = Rectangle {
            width: 5,
            height: 1,
        };

        assert!(larger.can_hold(&smaller));
    }

    #[test]
    fn smaller_cannot_hold_larger() {
        let larger = Rectangle {
            width: 8,
            height: 7,
        };
        let smaller = Rectangle {
            width: 5,
            height: 1,
        };

        assert!(!smaller.can_hold(&larger));
    }

    #[test]
    fn example_of_neq() {
        assert_eq!(1, 1, "Custom error message");
        assert_ne!(1, 2);

        fn greeting(name: &str) -> String {
            format!("Hello {}", name)
        }

        let result = greeting("Carol");
        assert!(
            result.contains("Carol"),
            "Greeting did not contain name, value was `{}`",
            result
        );
    }

    #[test]
    // #[should_panic]
    // It's enough to supply a substring of the panic msg to the "expected" property here.
    #[should_panic(expected = "Please supply something between 1 and 100.")]
    fn greater_than_100() {
        Guess::new(200);
    }

    fn test_with_result() -> Result<(), String> {
        #[allow(clippy::eq_op)]
        if 2 + 2 == 4 {
            Ok(())
        } else {
            Err(String::from("two plus two does not equal four"))
        }
    }
}

// Notes:
// Cargo test runner collects everything printed to the std output.
// Despite that it will not print it, only prints stuff if test fails!
//
// If outputs are needed for passed tests too:
// cargo test -- --show-output
//
// Running test targeted: cargo test greater_than ... we all know what would run, and it's super!
// To ignore a test just use the #[ignore] annotation.
// this runs ignored only: cargo test -- --ignored

// Unit tests are small and more focused, testing one module in isolation at a time, and can test private interfaces.
// "can test private interfaces" !

// #[cfg(test)] annotation is required for unit tests because they are in the same place as the
// non-test code so this sets up the compiler to exclude this code from the final bundle.

// Unit test convention:
// The convention is to create a module named tests in each file to contain the test functions
// and to annotate the module with cfg(test).

// Integration tests usually goes to a separate tests directory and only can reach the public API (just as expected).
// Each file in the tests directory will be a separate create, so importing stuff under testing is required.
// Important is THEY ARE SEPARATE CREATES!!
