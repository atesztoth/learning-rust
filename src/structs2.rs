#![allow(unused_doc_comments)]

pub fn main() {
    // Just to be here:

    // To take advantage of some code generation once can easily derive traits like
    // "Debug" using the derive decorator.

    #[derive(Debug)]
    struct Rectangle {
        width: u32,
        height: u32,
    }

    main();

    fn main() {
        let rect1 = Rectangle {
            width: 30,
            height: 50,
        };

        println!("rect1 is {:?}", rect1);
        // println!("rect1 is {}", rect1); // At this point I don't yet know how to implement traits. :(
        // Must continue learning.
        // I guess it's like that. One has to learn if one wants to know something. <mind blown (?)>

        let area = area(&rect1);

        println!("rect1 area is {}", area);

        // For small treats like this I feel like I MUST go through the documentation before doing
        // anything important:
        /**
        Another way to print out a value using the Debug format is to use the dbg! macro,
        which takes ownership of an expression (as opposed to println!, which takes a reference),
        prints the file and line number of where that
        dbg! macro call occurs in your code along with the resultant value of that expression,
        and returns ownership of the value.
         */

        dbg!(&rect1);

        // we can put dbg! around any expr because it returns ownership. Cool!
        Rectangle {
            height: dbg!(30 * rect1.height),
            width: 100,
        };
    }

    pub fn area(rectangle: &Rectangle) -> u32 {
        rectangle.width * rectangle.height
    }
}
