#![allow(dead_code)]
#![allow(unused_variables)]

fn main() -> () {
    // I won't write an example for everything, that's cumbersome and unnecessary.
    // But I'll take it as a not for sure:
    let decimal = 98_222;
    let hex = 0xff;
    let octal = 0o77;
    let binary = 0b1111_0000;
    let byte = b'A'; // (u8 only)

    // Architecture dependent variable type for number: isize	usize

    let arch_num: usize = 123143;

    // And one thing I'll love: "_" works as a separator
    let some_num = 1_000_000;

    //  Integer division truncates toward zero to the nearest integer.
    let truncated = -5 / 3; // Results in -1

    // Note for strings:
    // Note that we specify char literals with single quotes, as opposed to string literals, which use double quotes.

    // Tne product type, n-ary tuple exists here too. Cool!
    let tup: (i32, f64, u8) = (500, 6.4, 1);

    // oh, and we have destructuring!
    let (a, b, c) = tup;

    println!("Enum second element: {}", tup.1);

    // Arrays
    let months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];

    // full type for static array needs a length
    let a: [i32; 5] = [1, 2, 3, 4, 5];
}
