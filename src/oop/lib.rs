use std::fmt::Debug;

pub trait Draw {
    fn draw(&self);
}

// components has heap allocated, unknown sized elements that conforms to Draw.
// no generics, because that would result in a homogenous collection.
pub struct Screen {
    pub components: Vec<Box<dyn Draw>>,
}

impl Screen {
    pub fn run(&self) {
        for component in self.components.iter() {
            // dynamic dispatch will be used!
            // https://rust-book.cs.brown.edu/ch17-02-trait-objects.html#trait-objects-perform-dynamic-dispatch
            component.draw();
        }
    }
}

#[derive(Debug)]
pub struct Button {
    pub width: u32,
    pub height: u32,
    pub label: String,
}

impl Draw for Button {
    fn draw(&self) {
        println!("Drawing your button... {:?}", &self)
    }
}
