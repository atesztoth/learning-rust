// State pattern is when you have a set of state objects, and
// the main object can be in one of these states, and this current state object
// defines the behaviour of the object.

struct Post {
    state: Option<Box<dyn State>>,
    content: String,
}

impl Post {
    pub fn new() -> Post {
        Post {
            state: Some(Box::new(Draft {})),
            content: String::new(),
        }
    }

    pub fn add_text(&mut self, text: &str) {
        self.content.push_str(text);
    }

    pub fn content(&self) -> &str {
        // need to delete getting the content to the state,
        // so it can decide what to show
        // We call the as_ref method on the Option because we want a reference to the value
        // inside the Option rather than ownership of the value.
        self.state.as_ref().unwrap().content(self)
    }

    pub fn request_review(&mut self) {
        // take() takes the value of state, leaves a None after
        if let Some(s) = self.state.take() {
            self.state = Some(s.request_review())
        }
    }

    pub fn approve(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.approve())
        }
    }
}

trait State {
    fn request_review(self: Box<Self>) -> Box<dyn State>;
    fn approve(self: Box<Self>) -> Box<dyn State>;

    // See lifetimes part, that explains why
    // the returned value must have the same lifetime as the
    // post in param.

    // Ok actually here it is in short:
    // Note that we need lifetime annotations on this method.
    // We’re taking a reference to a post as an argument and returning a reference to part of that post,
    // so the lifetime of the returned reference is related to the lifetime of the post argument.
    fn content<'a>(&self, p: &'a Post) -> &'a str {
        &p.content
    }
}

struct Draft {}

impl State for Draft {
    // This syntax means the method is only valid when called on a Box holding the type.
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        Box::new(PendingReview {})
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn content<'a>(&self, _p: &'a Post) -> &'a str {
        ""
    }
}

struct PendingReview {}

impl State for PendingReview {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        Box::new(Published {})
    }

    fn content<'a>(&self, _p: &'a Post) -> &'a str {
        ""
    }
}

struct Published {}

impl State for Published {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }
}

pub fn main() {
    let mut post = Post::new();

    post.add_text("I ate a salad for lunch today");
    assert_eq!("", post.content());

    post.request_review();
    assert_eq!("", post.content());

    post.approve();
    assert_eq!("I ate a salad for lunch today", post.content());
}
