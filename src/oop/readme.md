# OOP notes

## No inheritance

```
If a language must have inheritance to be an object-oriented language,
then Rust is not one.
There is no way to define a struct that inherits the parent struct’s
fields and method implementations without using a macro.
```

This I was aware of from other places, but I consider it worthy mentioning:

```
Inheritance has recently fallen out of favor as a
programming design solution in many programming languages
because it’s often at risk of sharing more code than necessary.
Subclasses shouldn’t always share all characteristics of their parent class
but will do so with inheritance. This can make a program’s design less flexible.
It also introduces the possibility of calling methods on subclasses
that don’t make sense or that cause errors because the methods
don’t apply to the subclass. In addition, some languages will only allow
single inheritance (meaning a subclass can only inherit from one class),
further restricting the flexibility of a program’s design.
```