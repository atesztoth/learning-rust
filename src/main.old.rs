use std::io;
use rand::Rng;
use std::cmp::Ordering;

mod vars;
mod data_types;

fn main() {
    println!("Guess the number!");

    let secret_number: u32 = rand::thread_rng().gen_range(1..=100);

    println!("number: {}", secret_number);

    println!("Please input your guess!");

    loop {
        let mut guess = String::new();

        io::stdin().read_line(&mut guess).expect("Cannot read line!");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Failed to parse number!");
                continue;
            }
        };

        println!("You typed: {guess}");

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("The number is less than the secret one."),
            Ordering::Greater => println!("The number is greater than the secret one."),
            Ordering::Equal => {
                println!("YOU WIN!");
                break;
            }
        }
    }
}