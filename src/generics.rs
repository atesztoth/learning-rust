pub mod examples;
pub mod mad_structs_with_refs;
pub mod references_with_lifetimes;
pub mod traits;
pub mod traits_consumer;
