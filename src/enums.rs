#[allow(dead_code)]
#[allow(unused_variables)]
#[allow(unused_assignments)]
pub fn main() {
    enum IpAddressKind {
        V4,
        V6,
    }

    let four = IpAddressKind::V4;
    let six = IpAddressKind::V6;

    struct IpAddr {
        kind: IpAddressKind,
        address: String,
    }

    let _home = IpAddr {
        kind: four,
        address: String::from("127.0.0.1"),
    };

    let _loopback = IpAddr {
        kind: six,
        address: String::from("::1"),
    };

    // Ah yeah, enums can have associated values! Love it!
    enum EnhancedIpKind {
        V4(String),
        V6(String),
    }

    let _home = EnhancedIpKind::V4(String::from("127.0.0.1"));
    let _loopback = EnhancedIpKind::V6(String::from("::1"));

    #[derive(Debug)]
    enum AssocTypeExample {
        A(u8, u8, u8, u8),
        B(String),
    }

    let a = AssocTypeExample::A(1, 2, 3, 4);
    let b = AssocTypeExample::B(String::from("str"));

    dbg!(a);
    dbg!(b);

    // !!! props can even have named fields like Move does!
    enum Message {
        Quit,
        Move { x: i32, y: i32 },
        Write(String),
        ChangeColor(i32, i32, i32),
    }

    // Rust does not have nulls (https://doc.rust-lang.org/book/ch06-01-defining-an-enum.html#the-option-enum-and-its-advantages-over-null-values).
    // It uses something called "Optionals", which is implemented exactly the same way as Swift does it.
    enum Option<T> {
        None,
        Some(T),
    }

    // oh wait, swift uses snakeCase. That's the difference. :)

    let optional_str = Some(String::from("Rust"));

    // Check the types of these expressions.
    // Option<<unknown>> but the compiler will complain (cant infer the type) so I gave a type for it.
    // (had to explicitly write "Option" because the compiler thinks it's Option from std lib.)
    let _a: Option<()> = Option::None;

    let mut v = None; // since it is inferrable from the next line, it's Option<i32>. Love it!
    v = Some(5);

    dbg!(v);

    dbg!(optional_str);

    let example = Some(5);

    // But how to use that value?
    // https://doc.rust-lang.org/std/option/enum.Option.html
    assert_eq!(example.is_some(), true);

    // Pattern matching
    match example {
        None => {} // no op
        Some(v) => {
            // do something with v
            dbg!(v);
        }
    }

    dbg!(3 + example.unwrap());

    // You can chain it (Option is a monad it seems)
    // with functions like:
    // example.and_then( ... );

    // !! Cool example from Rust book !!:
    #[derive(Debug)] // so we can inspect the state in a minute
    enum UsState {
        Alabama,
        Alaska,
    }

    enum Coin {
        Penny,
        Nickel,
        Dime,
        Quarter(UsState),
    }

    fn value_in_cents(coin: Coin) -> u8 {
        match coin {
            Coin::Penny => {
                // Run some code here
                println!("Lucky penny!");
                1
            }
            Coin::Nickel => 5,
            Coin::Dime => 10,
            Coin::Quarter(state) => {
                println!("State quarter from {:?}!", state);
                50
            }
        }
    }

    fn is_a_penny(coin: Coin) -> bool {
        match coin {
            Coin::Penny => true,
            _ => false,
        }
    }

    // if - let
    // The syntax if let takes a pattern and an expression separated by an equal sign.
    // It works the same way as a match, where the expression is given to the match and the pattern is its first arm.
    // In this case, the pattern is Some(max), and the max binds to the value inside the Some.
    // We can then use max in the body of the if let block in the same way we used max in the corresponding match arm.
    // The code in the if let block isn’t run if the value doesn’t match the pattern.
    let config_max = Some(3u8);
    if let Some(max) = config_max {
        println!("The maximum is configured to be {}", max);
    }
}
