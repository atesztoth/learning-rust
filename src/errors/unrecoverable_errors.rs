// Error handing mostly done using an "Either type" called "Result",
// ... ok the point is the theory. If an error can be expected, it's recoverable (handle it yourself),
// otherwise it's panic! (really an error) (like out-indexing an array, which really is an error).

// When a panic happens Rust will walk up the stack and collect info about the error (line no etc.)
// but yeah that is very expensive. This can be turned off if binary size matters: (cargo.toml:)
// [profile.release]
// panic = 'abort'

use std::error::Error;
use std::fs::File;
use std::io::{self, ErrorKind, Read};

pub fn main() -> Result<(), Box<dyn Error>> {
    // panic!("BIG TIME PANIC");
    let open_res = File::open("nah-boi-no-file");

    let file: Option<File> = match open_res {
        Ok(f) => {
            println!("lol the file existed. xd");
            dbg!(&f);
            Some(f)
        }
        Err(e) => match e.kind() {
            ErrorKind::NotFound => {
                println!("File does not exist!");
                None
            }
            other => {
                println!("Unexpected error! {:?}", other);
                None
            }
        },
    };

    dbg!(file);

    // unwrap will panic if something unexpected happens.
    // it would be something like force-unwrap in Swift.
    // let greeting_file = File::open("hello.txt").unwrap();

    // .expect is unwrap but lets you define an error message.
    // let greeting_file = File::open("hello.txt")
    //     .expect("hello.txt should be included in this project");

    // The "Error propagation" part is very straightforward for me, I don't want to waste time on it.
    // The point is to return an "either type" with an error and let the caller decide what to do with it.

    // The "?" operator is a funny beast tho. Whichever error type that has an implementation of the
    // "From" trait for it can have the error converted to it by the "?" operator.

    // https://doc.rust-lang.org/book/ch09-02-recoverable-errors-with-result.html#where-the--operator-can-be-used
    // The ? operator can only be used in functions whose return type is compatible with the value the ? is used on.
    // This is because the ? operator is defined to perform an early return of a value out of the function

    let result = read_username_from_file();

    println!("{}", result.unwrap_or("nyeh".to_string()));

    // But yeah something like this is perfectly possible:
    // But no mix match like using a "?" on a Result, then on an Option ... etc

    // let res: Result<String, Box<dyn Error>> = Ok("str".to_string());
    // println!("{:?}", res?.to_uppercase());
    println!(
        "{:?}",
        (Ok("str".to_string()) as core::result::Result<String, Box<dyn Error>>)?.to_uppercase()
    );

    // (Why the type of the Result created using the Ok ctor had to match the return type of the main fn?
    // because the "?" operator would return the error if occurred, read the stuff in "read_username_from_file" fn!)

    Ok(())
}

fn read_username_from_file() -> Result<String, io::Error> {
    // !!!!!!!!!!!!
    // The ? placed after a Result value is defined to work in almost the same way as the match expressions
    // we defined to handle the Result values in Listing 9-6. If the value of the Result is an Ok,
    // the value inside the Ok will get returned from this expression, and the program will continue.
    // If the value is an Err, the Err will be returned from the whole function as if we had used the
    // return keyword so the error value gets propagated to the calling code.
    // !!!!!!!!!!!!

    let mut username = String::new();

    File::open("hello.txt")?.read_to_string(&mut username)?;

    Ok(username)

    // What has done above is a common thing to do, so the std lib provides a func for it (God bless):
    // fs::read_to_string("hello.txt")

    // But this lengthy one stays because the purpose now is to show error handling techniques not the std lib.
}
