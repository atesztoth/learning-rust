use rand::Rng;
use std::cmp::Ordering;
use std::io;

pub fn main() {
    println!("[STRICT] Guess the number!");

    let secret_number: u32 = rand::thread_rng().gen_range(1..=100);

    println!("number: {}", secret_number);

    println!("Please input your guess! Supplying crazy guesses will cause a crash!");

    loop {
        let mut guess = "".to_string();

        io::stdin()
            .read_line(&mut guess)
            .expect("Cannot read line!");

        let guess = match guess.trim().parse() {
            Ok(num) => Guess::new(num),
            Err(_) => {
                println!("Failed to parse number!");
                continue;
            }
        };

        println!("You typed: {}", guess.value);

        match guess.value.cmp(&secret_number) {
            Ordering::Less => println!("The number is less than the secret one."),
            Ordering::Greater => println!("The number is greater than the secret one."),
            Ordering::Equal => {
                println!("YOU WIN!");
                break;
            }
        }
    }
}

pub struct Guess {
    value: u32,
}

impl Guess {
    pub fn new(value: u32) -> Guess {
        if value < 1 || value > 100 {
            panic!(
                "Invalid value! Please supply something between 1 and 100. Supplied: {}",
                value
            );
        }

        Guess { value }
    }

    pub fn value(&self) -> u32 {
        self.value
    }
}
