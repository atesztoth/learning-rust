// Collections should be very familiar already regardless of where's one's coming from.
// Even if someone's of the highest levels of culture and built up the fundamentals of
// a list starting from semigroups, or just presented a List<T>, these should be familiar.

// Type annotation is added here because type is not inferrable without elements.
// (An example of this can be seen in enums.rs.)

#[allow(unused_variables)]
pub fn main() {
    println!("Collections / vectors");

    let mut v: Vec<i32> = Vec::new();

    // add an element...
    v.push(1);

    // definition with elements
    let v = vec![1, 2, 3];

    let third: &i32 = &v[2]; // ! type
    dbg!(third);

    // this .get will be my friend I already can see:
    // (!! type is Option<&i32>, getting a reference to the element !!)
    let third = v.get(2);

    match third {
        None => {
            println!("No third element. Sad story bra. :(")
        }
        Some(v) => {
            println!("Third element be like {}", v);
        }
    }

    // over-indexing stuff
    // let lottath = v[100];
    // dbg!(lottath); // panic
    // we know how .get would work.
    dbg!(v.get(100).is_some());

    // iteration 101
    let v = vec![1, 100, 542];

    for i in &v {
        println!("{}", i);
    }

    // Note: using this scope v will be dropped after iterating over. (goes out of scope)
    {
        let mut v = vec![1, 2, 3];
        for i in &mut v {
            *i += *i + 1;
            println!("{}", i);
        }
    }
}
