use std::collections::HashMap;
use std::ptr::slice_from_raw_parts;

// This collection is homogenous. Meaning if given HashMap<K, V>, K, V cannot change. (Should this be surprising?)
pub fn main() {
    println!("Collections / hash maps");

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    let score = scores.get(&"Blue".to_string()).copied().unwrap_or(0);

    println!("Score of blue team is {}", score);

    for (key, value) in &scores {
        println!("{key}: {value}");
    }

    // Minding ownership rules are extremely important in these cases

    let field_name = String::from("Favorite color");
    let field_value = String::from("Blue");

    let mut map = HashMap::new();
    map.insert(field_name, field_value);

    // dbg!(field_name); // ERROR: use of moved value: `field_name`

    // Overwriting values works just as expected
    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Blue"), 25);

    println!("{:?}", scores);

    // Adding a Key and Value Only If a Key Isn’t Present:
    // entry returns an enum called Entry that represents a value that may or may not exist.
    // Holy sh* I just love this API.
    scores.entry(String::from("Yellow")).or_insert(50);

    println!("{:?}", scores);

    // Updating a Value Based on the Old Value
    let old_value = scores.entry("Blue".to_string()).or_insert(0); // Because it returns a mutable ref...
                                                                   // no need to do anything else just deref
    *old_value = 999;

    println!("{:?}", scores);

    // split_whitespace
    // just know this exists!
    let iterator = "hello split me at whitespaces please".split_whitespace();
    iterator.for_each(|slice| {
        println!("{}", slice);
    });
}
