// Structure definitions and lifetimes.

// OFC here also the problem of lifetimes comes up as soon as a struct hold references.

use std::fmt::Display;

struct ImportantExcerpt<'a> {
    part: &'a str,
}

pub fn main() {
    // Just an example on how the reference to the string slice exists until _i goes out of scope so it's valid.
    let novel = String::from("Call me Ishmael. Some years ago...");
    let first_sentence = novel.split('.').next().expect("Could not find a '.'");
    let _i = ImportantExcerpt {
        part: first_sentence,
    };
}

// Next is a reading: https://doc.rust-lang.org/book/ch10-03-lifetime-syntax.html#lifetime-elision
// Then: https://doc.rust-lang.org/book/ch10-03-lifetime-syntax.html#lifetime-annotations-in-method-definitions

// See placement of the lifetime tags:
impl<'q> ImportantExcerpt<'q> {
    // Lifetime elision rules kick in, no need for specifying lifetime tags.
    fn level(&self) -> i32 {
        return 3;
    }

    // 3rd elision rule (one of the references is &self so common lifetime is lifetime of self)
    fn announce_and_return_part(&self, announcement: &str) -> &str {
        println!("Attention please: {}", announcement);
        self.part
    }
}

// THE STATIC LIFETIME

fn examples() {
    // This lifetime denotes that the reference can live for the entire duration of the program.
    let _s: &'static str = "No killing me compiler boi.";
    // The text of this string is stored directly in the program’s binary, which is always available.
    // Therefore, the lifetime of all string literals is 'static.
}

// The biggest house party when everybody comes together:
// https://doc.rust-lang.org/book/ch10-03-lifetime-syntax.html#generic-type-parameters-trait-bounds-and-lifetimes-together
// Generic Type Parameters, Trait Bounds, and Lifetimes Together

// Lifetime annotations come first in the type nam declaration list.
fn longest_with_an_announcement<'a, T>(x: &'a str, y: &'a str, ann: T) -> &'a str
where
    T: Display,
{
    println!("Announcement! {}", ann);
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
