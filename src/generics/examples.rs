// These are just going to be examples.
// A nice refactor flow can be read in the Rust book...

use std::any::Any;

pub fn main() {}

// Here an example can be seen how to require type params to implement traits.
// The least requirement to use a comparison operator is to have the elements of a collection
// to be able to be actually compared to each other. This means the collection has to have a
// relation defined between its elements that conforms to the rules of a partial ordering.
fn lin_search_largest<T: std::cmp::PartialOrd>(list: &[T]) -> &T {
    let mut largest = &list[0];

    for item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}

// Example of a struct using generic type parameter:
struct Pair<X, Y> {
    fst: X,
    snd: Y,
}

// enum:
enum Maybe<A> {
    Just(A),
    Nothing,
}

enum Either<A, B> {
    Left(A),
    Right(B),
}

// implementation clause
impl<X, Y> Pair<X, Y> {
    fn get_first(&self) -> &X {
        &self.fst
    }
}

// doing implementations for specific types are also possible:
impl Pair<String, String> {
    fn concatenate_elements(&self) -> String {
        "".to_string() + &self.fst
    }
}

// Point
struct Point<X1, Y1> {
    x: X1,
    y: Y1,
}

impl<T, T2> Point<T, T2> {
    fn x(&self) -> &T {
        &self.x
    }
}

impl Point<f32, f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}

impl<X1, Y1> Point<X1, Y1> {
    // Note the usage of X2, Y2 generic type parameters
    fn mixup<X2, Y2>(self, other: Point<X2, Y2>) -> Point<X1, Y2> {
        Point {
            x: self.x,
            y: other.y,
        }
    }
}
