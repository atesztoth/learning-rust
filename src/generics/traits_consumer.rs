// example of how to use traits from traits.rs

use super::traits::{in_short, NewsArticle, Summary, Tweet};

// One restriction to note is that we can implement a trait on a type only if at least one of the trait or the type is local to our crate.
// We can’t implement external traits on external types.
// For example, we can’t implement the Display trait on Vec<T> within our generics crate,
// because Display and Vec<T> are both defined in the standard library and aren’t local to our generics crate.
// This restriction is part of a property called coherence, and more specifically the orphan rule,
// so named because the parent type is not present.
// This rule ensures that other people’s code can’t break your code and vice versa.
// Without the rule, two crates could implement the same trait for the same type, and Rust wouldn’t know which implementation to use.

pub fn main() {
    let tweet = Tweet {
        username: String::from("Rustacean Károly"),
        content: String::from("I will say something people should quote from me!"),
        reply: false,
        retweet: false,
    };

    let important_news = NewsArticle {
        headline: "There is a kind of Rust you don't want to get rid of".to_string(),
        location: "Wordwide".to_string(),
        author: String::from("James Da Reporter"),
        content: String::from("I know, I know. This is ridiculous, but it is true."),
    };

    println!(
        "That's a bit funky but to use the trait implementation one must bring that into scope: {}",
        tweet.summarize()
    );

    println!("{}", tweet.extended_summary());

    ////
    println!("Shorts:");

    // This will be too much to explain why I did.
    let summaries = vec![in_short(&tweet), in_short(&important_news)];
    summaries.iter().for_each(|f| *f);
}
