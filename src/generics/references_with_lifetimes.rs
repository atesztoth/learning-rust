// Lifetimes can be thought about as another kind of generics.
// The difference is rather than ensuring that a type has a specific behavior,
// it ensures that the references are valid as long as needed.

// Every reference has a lifetime associated with it. In most cases it is inferred automatically.
// Lifetimes must be annotated when lifetimes of references can be related in a few different ways.
// Rust requires to annotate the relationships using generic lifetime parameters
// to ensure the actual references used at runtimes will definitely be valid.

// Quite a mouthful, huh?

// Should be read carefully: https://doc.rust-lang.org/book/ch10-03-lifetime-syntax.html !

pub fn main() {
    // Read this afterward the comments below.

    let string1 = String::from("long string is long");

    {
        let string2 = String::from("xyz");
        let result = longest(string1.as_str(), string2.as_str());
        println!("The longest string is \"{}\"", result);
    }

    // The following example shows what happens when the lifetimes of the shortest (string2)
    // lifetime is not long enough. Remember, all annotated with 'a, so the shortest will be taken.
    // That is string2, but that's not long enough. Does not live as long as result.
    // We can see if we think that string 1 reference will be in result which is good,
    // but please... What kind of security is this? What if we change the text? That's a joke, the compiler is right.
    // string 2 would go out of scope too soon to be used if needed hence it would be a place for crash. whoops

    // "Next, let’s try an example that shows that the lifetime of the reference in result must be the smaller lifetime of the two arguments."

    // let string1 = String::from("long string is long");
    // let result;
    // {
    //     let string2 = String::from("xyz");
    //     result = longest(string1.as_str(), string2.as_str());
    // }
    // println!("The longest string is {}", result);
}

// A function that returns the reference holding the longest string.
// Relevant long text:
// When we’re defining this function, we don’t know the concrete values that will be passed into this function,
// so we don’t know whether the if case or the else case will execute.
// We also don’t know the concrete lifetimes of the references that will be passed in,
// so we can’t look at the scopes as we did in Listings 10-17 and 10-18 to determine whether the reference we return will always be valid.
// The borrow checker can’t determine this either,
// because it doesn’t know how the lifetimes of x and y relate to the lifetime of the return value.
// To fix this error, we’ll add generic lifetime parameters that define the relationship between the references
// so the borrow checker can perform its analysis.

// Very importante:
// Lifetime annotations don’t change how long any of the references live.
// Rather, they describe the relationships of the lifetimes of multiple references to each other without affecting the lifetimes.

// So lifetime params are basically labels. You can label each reference and hence kind of group them
// and by putting the reference labels to places when you want to see a labelled reference to be valid.
// E.g.: Labelled everything with "a" so everything should be valid as long as the others.
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        return x;
    }
    y
}

// The lifetime annotations become part of the contract of the function...
// lol feels like I am reading something about Eiffel... https://www.eiffel.org/doc/solutions/Design_by_Contract_and_Assertions

/////// Thinking in Terms of Lifetimes
// There is no way we can specify lifetime parameters that would change the dangling reference,
// and Rust won’t let us create a dangling reference.
// In this case, the best fix would be to return an owned data type
// rather than a reference so the calling function is then responsible for cleaning up the value.

// Bad example:
// error[E0515]: cannot return value referencing local variable `result`
// Bad because the resource "result" is owned by bad_example, and when the scope of it is over,
// this resource would go out of scope (also known as being removed) so it would create a dangling reference.
// fn bad_example<'a>() -> &'a str {
//     let result = String::from("really long string");
//     result.as_str()
// }

// But yeah as the text said above, just return an owned type e.g. a String and let the caller do the cleanup.
fn example() -> String {
    return "hello".to_string();
}
