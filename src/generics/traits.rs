use std::fmt::{format, Debug, Display};
use std::iter::Sum;

pub fn main() {}

// Traits are like interfaces, but there are some differences.
pub trait Summary {
    // Each type implementing this trait should provide its implementation.
    // Question: Default implementations? Hint: Yeah, just give one by giving the function a body.
    fn summarize(&self) -> String;

    // Some default implementation stuff:
    fn extended_summary(&self) -> String {
        self.summarize()
    }
}

// Taking the perfect example from Rust book:
pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

// Example of implementation
impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!(
            "Summary: {}, by {} ({})",
            self.headline, self.author, self.location
        )
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Tweet {
    fn summarize_author(&self) -> String {
        format!("@{}", self.username)
    }
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{} - {}", self.content, self.summarize_author())
    }
}

// This is how one can tell the compiler one wants to accept types that conforms to some trait:
pub fn in_short(item: &impl Summary) {
    println!("Today on \"in short\": {}", item.summarize())
}

// Sweet? Right but that was just a syntactic sugar for the "Bound syntax".
// Yeah, they only said after you ate that it contained sugar. Rude people, very rude!
// For some reason, I don't know why I like to use the most verbose form of stuff when I see them
// for the first time, only from the second time I am aiming for making the code actually the prettiest.
// This means... Yeah. The Bound Syntax!
// Ok this syntax I've seen a million times so the previous one will be in use.
// pub fn in_short<T: Summary>(item: &T)  {
//     println!("Today on \"in short\": {}", item.summarize())
// }

// A more funny thing to see how to expect a param to implement multiple traits:
pub fn in_short2(item: &(impl Summary + Display)) {
    println!("ITEM 2 RELOADED, {}", item.summarize());
}

// OR:
// pub fn in_short2<T: Summary + Display>(item: &T) {
//     println!("ITEM 2 RELOADED, {}", item.summarize());
// }

// Clearer Trait Bounds with where Clauses:
fn example_fun<T: Display + Clone, U: Clone + Debug>(t: &T, u: &U) -> i32 {
    dbg!(u);
    println!("{}", &t);
    5
}

fn example_fun2<T, U>(t: &T, u: &U) -> i32
where
    T: Display + Clone,
    U: Debug,
{
    dbg!(u);
    println!("{}", &t);
    5
}

// Returning types that implement traits:
fn create_sum_tweet() -> impl Summary {
    Tweet {
        username: String::from("Tweeter"),
        content: String::from("Very important tweeto."),
        reply: false,
        retweet: false,
    }
}

// This won't compile:
// Later will be shown a solution how to do stuff like this.
// fn returns_summarizable(switch: bool) -> impl Summary {
//     if switch {
//         NewsArticle {
//             headline: String::from(
//                 "Penguins win the Stanley Cup Championship!",
//             ),
//             location: String::from("Pittsburgh, PA, USA"),
//             author: String::from("Iceburgh"),
//             content: String::from(
//                 "The Pittsburgh Penguins once again are the best \
//                  hockey team in the NHL.",
//             ),
//         }
//     } else {
//         Tweet {
//             username: String::from("horse_ebooks"),
//             content: String::from(
//                 "of course, as you probably already know, people",
//             ),
//             reply: false,
//             retweet: false,
//         }
//     }
// }

// Using Trait Bounds to Conditionally Implement Methods
// This is very cool, specific implementation for types that have some characteristics.

struct Pair<T> {
    x: T,
    y: T,
}

impl<T> Pair<T> {
    fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T: Display + PartialOrd> Pair<T> {
    fn cmp_display(&self) {
        if self.x >= self.y {
            println!("The largest member is x = {}", self.x);
        } else {
            println!("The largest member is y = {}", self.y);
        }
    }
}

// We can also conditionally implement a trait for any type that implements another trait.
// Implementations of a trait on any type that satisfies the trait bounds are called
// --> blanket implementations <-- and are extensively used in the Rust standard library.
// For example, the standard library implements the ToString trait on any type that implements the Display trait.
// The impl block in the standard library looks similar to this code:
// impl<T: Display> ToString for T {
//     // --snip--
// }

// Some mocked example:
trait Printable<T> {
    fn print_me(&self) -> String;
}

impl<T: Debug> Printable<T> for T
where
    T: Display,
{
    fn print_me(&self) -> String {
        format!("Debuggable stuff! {}", dbg!(self))
    }
}
