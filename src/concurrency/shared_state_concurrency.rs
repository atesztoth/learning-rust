use std::rc::Rc;
use std::sync::{Arc, Mutex};
use std::thread;

// mutex example

// I'm not commenting that much now because I kind of got bored by writing much and progressing slower.
// I'll rather just copy some important stuff from the book.

// Mutex is an abbreviation for mutual exclusion, as in, a mutex allows only one thread
// to access some data at any given time. To access the data in a mutex, a thread must first signal
// that it wants access by asking to acquire the mutex’s lock.
// The lock is a data structure that is part of the mutex that keeps track of who currently has
// exclusive access to the data. Therefore,
// the mutex is described as guarding the data it holds via the locking system.
pub fn main() {
    example_mutex_introduction();
    example_multi_thread_increment();
}

// Using mutex in a single threaded context, just to show the API.
fn example_mutex_introduction() {
    let m = Mutex::new(5);

    {
        // lock blocks
        let mut v = m.lock().unwrap();
        *v = 10;

        // The call to lock would fail if another thread holding the lock panicked.
        // In that case, no one would ever be able to get the lock,
        // so we’ve chosen to unwrap and have this thread panic if we’re in that situation.

        // Mutex has Deref, and Drop traits implemented for expectable reasons.
    }

    println!("m = {:?}", m);
}

fn example_multi_thread_increment() {
    // Arc is the thread safe version of RC. Name stands for Atomic Reference Counting.
    let value = Arc::new(Mutex::new(0u8));
    let mut handles = vec![];

    for _ in 0..10 {
        let value = Arc::clone(&value);
        let handle = thread::spawn(move || {
            let mut num = value.lock().unwrap();
            *num += 1;
        });

        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Result {}", *value.lock().unwrap());
}
