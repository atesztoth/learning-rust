// The Rust standard library uses a 1:1 model of thread implementation, whereby a program uses one operating system thread per one language thread. There are crates that implement other models of threading that make different tradeoffs to the 1:1 model.

use std::thread;
use std::time::Duration;

fn first_example() {
    // threads can be spawned using std::thread::spawn.
    thread::spawn(|| {
        for i in 1..10 {
            println!("this chapter will be very exciting! spawned thread: {}", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("number from main thread {}", i);
        thread::sleep(Duration::from_millis(1));
    }

    // Outputted only till 5 from main thread and before the spawned could have finished!
    // That is no accident, it's because when the main thread finishes all the other threads will be shut down.
    // Btw the output differs based on how the OS schedules the threads.
    // This is a serious thing, even that the spawned thread will be run at all is not subject of guarantee!
}

pub fn main() {
    let join_handle = thread::spawn(|| {
        for i in 1..10 {
            println!("spawned thread: {}", i);
            thread::sleep(Duration::from_millis(1));
        }

        5
    });

    // join_handle.join().unwrap(); // leave it here while leaving the other call there, see
    // the error msg.

    // doing this here would produce a better console output:
    // let x = join_handle.join().unwrap();

    for i in 1..5 {
        println!("number from main thread {}", i);
        thread::sleep(Duration::from_millis(1));
    }

    // A JoinHandle is an owned value. When join() is called on it, it'll wait for
    // it's thread to finish.
    let x = join_handle.join().unwrap(); // join_handle.join() will run on the current thread
                                         // in this case it's the main thread. It blocks the current thread until the thread
                                         // that the handle belongs to finishes!
                                         // If it was not like that return values from threads would be a bit ... a-void-ed (oh bad joke man)
    println!("Return value from thread {}", x);

    data_usage();
    data_usage2()
}

fn data_usage() {
    let v = vec![1, 2, 3];

    // let handle = thread::spawn(|| {
    //     println!("vector: {:?}", v); // error: note: function requires argument type to outlive `'static`
    //     // That is impossible. Outliving static memory is not possible, that is a memory space allocated
    //     // at the start of running of a program, and deallocated when it finishes.
    //     // Rust does not have a way to know how long this thread would live, or the main thread, so it cannot allow this borrow.
    // });

    // using the move keyword the closure is forced to take ownership of the
    // captured values, hence v, rather than letting Rust infer if it should borrow or mut borrow or move.
    let handle = thread::spawn(move || {
        println!("vector: {:?}", v); // but move the vector, and you'll be happy!
    });

    handle.join().unwrap()
}

fn data_usage2() {
    let v = vec![1, 2, 3];

    let handle = thread::spawn(move || {
        println!("vec: {:?}", v);
    });

    // drop(v); // Will not work. Why?
    // Because v is moved into the territory of the spawned thread,
    // this means this thread has nothing to do with v anymore.

    // (Returning v from the thread and then dropping would work.)

    handle.join().unwrap();
}
