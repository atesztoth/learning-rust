// Channels

use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// mpsc: multiple producer, single consumer

pub fn main() {
    // (transmitter, receiver)
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let val = String::from("hi");
        let result = tx.send(val);

        match result {
            Err(e) => {
                println!(
                    "Error while sending message through mpsc channel! {}",
                    e.to_string()
                )
            }
            _ => {}
        }
    });

    // We’re using recv, short for receive, which will block the main thread’s execution
    // and wait until a value is sent down the channel.
    // Once a value is sent, recv will return it in a Result<T, E>.
    // When the transmitter closes,
    // recv will return an error to signal that no more values will be coming.
    let received = rx.recv().unwrap();
    println!("Got: {}", received);

    // The try_recv method doesn’t block, but will instead return a Result<T, E> immediately:
    // an Ok value holding a message if one is available and an Err value if there aren’t any messages this time.
    // Using try_recv is useful if this thread has other work to do while waiting for messages:
    // we could write a loop that calls try_recv every so often, handles a message if one is available,
    // and otherwise does other work for a little while until checking again.

    example_multi_val();
    example_multi_producer();
}

fn example_multi_val() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received in rx {
        println!("Got: {}", received);
    }
}

// Creating Multiple Producers by Cloning the Transmitter
fn example_multi_producer() {
    let (tx, rx) = mpsc::channel();

    // The transmitter can be cloned.
    let tx1 = tx.clone();
    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx1.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    thread::spawn(move || {
        let vals = vec![
            String::from("more"),
            String::from("messages"),
            String::from("for"),
            String::from("you"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received in rx {
        println!("Got: {}", received);
    }
}
