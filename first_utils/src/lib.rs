mod worker;

use std::sync::{mpsc, Arc, Mutex};
use worker::{Job, Worker};

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: Option<mpsc::Sender<Job>>,
}

impl ThreadPool {
    /// Creates a new threadpool.
    ///
    /// Number of threads in the pool.
    ///
    /// # Panics
    ///
    /// The `new` function will panic if the number of threads is zero.
    pub fn new(thread_num: usize) -> Self {
        assert!(thread_num > 0);

        let (sender, receiver) = mpsc::channel();

        // Want to share the receiver with the workers, but avoid race conditions as well.
        let receiver = Arc::new(Mutex::new(receiver));

        let mut workers = Vec::with_capacity(thread_num);

        for id in 0..4 {
            // wow, the shorthand syntax still works, cool!
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }

        ThreadPool {
            sender: Some(sender),
            workers,
        }
    }

    pub fn execute<F>(&self, f: F)
    where
        // F: FnOnce() -> () + Send + 'static, // which is equivalent to
        F: FnOnce() + Send + 'static, // https://stackoverflow.com/a/70489679/9720799
    {
        let job = Box::new(f);

        // If it doesn't have a sender when a job came, I prefer to make it fail.
        // At least for this implementation, I very much do.
        self.sender.as_ref().unwrap().send(job).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        // Be careful! After this drop, any calls to "recv" will return an error!
        // This must be taken care of in the worker.
        drop(self.sender.take());

        for worker in &mut self.workers {
            println!("Shutting down worker {}", worker.id);

            worker.shut_down();
        }
    }
}
