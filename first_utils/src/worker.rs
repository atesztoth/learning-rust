use mpsc::Receiver;
use std::sync::{mpsc, Arc, Mutex};
use std::thread;

// Here this 'static lifetime does not mean it'll
// live as long as the program. Rather, this expresses
// a restriction on the lifetime of values that the closure captures.
pub type Job = Box<dyn FnOnce() + Send + 'static>;

pub struct Worker<T = ()> {
    pub id: usize,
    thread: Option<thread::JoinHandle<T>>, // I would have called it handle but whatever
}

impl Worker {
    pub fn new(id: usize, receiver: Arc<Mutex<Receiver<Job>>>) -> Worker {
        // NOTE: When the OS is not able to create new threads,
        // this code would panic. Fortunately, this is not production code.
        let thread = thread::spawn(move || loop {
            // Don't forget: lock blocks, also recv blocks.
            // Also, the mutex lock guarantees that only one thread is trying to get
            // a job at a time.

            // Acquiring a lock might fail if the mutex is in a poisoned state,
            // which can happen if some other thread panicked
            // while holding the lock rather than releasing the lock.
            let job = receiver.lock().unwrap().recv();

            if job.is_err() {
                println!("Worker {id} is disconnected. Shutting down");
                return;
            }

            println!("Worker {id} got a job; executing.");

            job.unwrap()();
        });

        Worker {
            id,
            thread: Some(thread),
        }
    }

    pub fn shut_down(&mut self) {
        if let Some(thread) = self.thread.take() {
            // Note: join will only close the thread if it can finish it job.
            // (So the forever-looping implementation can cause it to stay open!)
            thread.join().unwrap();
        }
    }
}
