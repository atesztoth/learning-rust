# Learning Rust

In this repository I'm putting random code examples in which I test
out Rust stuff, some smaller programs and examples.

## Disclaimer

This repository contains a lot of my own comments and thoughts on things. \
These comments can be some random stuff not interesting to you at all but
I guess it's like that when you're learning.

_Module system is not covered in detail._ (It might be true for other parts.)

That is to increase my very low speed of progressing with stuff. I don't have much time, unfortunately.

## Sources

I am progressing by going through the Rust Book. \
Recently I came across an interactive version of the book that has questions inside, and after each chapter.

I find it very useful, so I'll share the link to that here.

O.G. Rust book: https://doc.rust-lang.org/book \
Interactive Rust book: https://rust-book.cs.brown.edu

## Notes

### `web-server` what is this doing here?

That is the 20th chapter in the book, and I want to keep as much as I can in this repo that is
related to the book.

To build or run a workspace member (a package):

- `cargo run -p web-server`
- `cargo build -p web-server`

The default package can be run in the "expected" way:

- `cargo run`
- `cargo build`

No params = default package.

### Dev stuff

Run the main carte in watch mode: `cargo watch -x run` \
Run the web server in watch mode: `cargo watch -x "run -p web-server"`
